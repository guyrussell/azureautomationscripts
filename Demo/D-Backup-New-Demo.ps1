

    $connectionName = "AzureRunAsConnection"
       
    try 
    {
        # Get the connection "AzureRunAsConnection "
        $servicePrincipalConnection = Get-AutomationConnection -Name $connectionName
 
        "Login to Azure"
 
        Add-AzureRmAccount `
        -ServicePrincipal `
        -TenantId $servicePrincipalConnection.TenantId `
        -ApplicationId $servicePrincipalConnection.ApplicationId `
        -CertificateThumbprint $servicePrincipalConnection.CertificateThumbprint
    }
 
    catch 
    {
        if (!$servicePrincipalConnection) 
        {
            $ErrorMessage = "Connection $connectionName not found."
            throw $ErrorMessage
        }
        else 
        {
            Write-Error -Message $_.Exception
            throw $_.Exception
        }
    }


    $SendGridUsername =(Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "Send-Grid-User-Name").SecretValueText
    $SendGridPassword = ConvertTo-SecureString -String (Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "Send-Grid-Password").SecretValueText -AsPlainText -Force
    $credential = New-Object System.Management.Automation.PSCredential $SendGridUsername, $SendGridPassword
    $SMTPServer = "smtp.sendgrid.net"
    $EmailFrom = "automated@vuemed.com"
    $EmailTo = "grussell@vuemed.com"

    #send me the start time
    $date = ((Get-Date).AddHours(-7)).ToString("yyyy-MM-dd-HH-mm")
    $Subject = "backup of demo started"
    $Body = "<p>backup prod copy as demo to storage</p><p>rename prod copy to demo</p><p>if either of last 2 fail, restore last good demo as demo from bacpac</p><p>$date Pacific Daylight Time</p>"
    Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml

    Try
    {
        $ServerName = "vuemeddb"

        $DatabaseName = "ProdCopy"

        $ResourceGroupName = "VueMed_Demo_Resource_Group"
        
        $ServerAdmin = (Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "Demo-Database-UserName").SecretValueText
        
        $SecurePassword = ConvertTo-SecureString -String (Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "Demo-Database-Password").SecretValueText -AsPlainText -Force

        $StorageKeytype = "StorageAccessKey"
        
        $StorageKey = (Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "databasebackuphot-storage-access-key").SecretValueText

        $BaseStorageUri = "https://databasebackuphot.blob.core.windows.net/demobackups/"
        
        # rename the newly minted ProdCopy for storage as BACPAC
        $bacpacFilename = "VueMedDemo-" + ((Get-Date).AddHours(-7)).ToString("yyyy-MM-dd-HH-mm") + ".bacpac"

        $BacpacUri = $BaseStorageUri + $bacpacFilename

        $exportRequest = New-AzureRmSqlDatabaseExport -ResourceGroupName $ResourceGroupName -ServerName $ServerName `
            -DatabaseName $DatabaseName -StorageKeytype $StorageKeytype -StorageKey $StorageKey -StorageUri $BacpacUri `
            -AdministratorLogin $ServerAdmin -AdministratorLoginPassword $SecurePassword
    }
    Catch
    {
        $Subject = "backup of ProdCopy failed!"
        $Body = "<p>backup of ProdCopy failed!</p><p>$date Pacific Daylight Time</p><p>$error</p>"
        Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
    }

#     ############################################## Rename Databases #######################################################
    
    # $connectionString = (Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "VueMedDatabaseMasterConnectionString").SecretValueText
    # $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
    # $command = new-object system.data.sqlclient.sqlcommand("",$connection)
    # $connection.Open()

    # Try
    # {
    #     $date = ((Get-Date).AddHours(-7)).ToString("yyyy_MM_dd_HH_mm")
    #     $newName = "VueMedDemo_$date"
    #     $command.CommandText = "ALTER DATABASE VueMedDemo MODIFY Name = $newName"
    #     $command.CommandTimeout = 3600
    #     $command.ExecuteNonQuery()

    #     Start-Sleep -s 60

    #     $command.CommandText = "ALTER DATABASE ProdCopy MODIFY Name = VueMedDemo"
    #     $command.CommandTimeout = 3600
    #     $command.ExecuteNonQuery()

    #     $connection.Close()
    
    #     Start-Sleep -s 900 
    
    #     # resize old demo to lowest size
    #     Set-AzureRmSqlDatabase -ResourceGroupName $ResourceGroupName -ServerName $ServerName -DatabaseName $newName -Edition "Standard" -RequestedServiceObjectiveName "S0"
    #     # resize new demo to S1 until you hear from management
    #     Set-AzureRmSqlDatabase -ResourceGroupName $ResourceGroupName -ServerName $ServerName -DatabaseName "VueMedDemo" -Edition "Standard" -RequestedServiceObjectiveName "S1"
        
    #     $date = ((Get-Date).AddHours(-7)).ToString("yyyy-MM-dd-HH-mm")
    #     $Subject = "rename databases complete"
    #     $Body = "<p>everything okey doke</p><p>At $date</p>"
    #     Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
    # }
    # Catch
    # {
    #     $date = ((Get-Date).AddHours(-7)).ToString("yyyy-MM-dd-HH-mm")
    #     $Subject = "rename databases failed!"
    #     $Body = "<p>everything NOT okey doke</p><p>At $date</p><p>$error</p>"
    #     Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
    # }
    # Finally
    # {
    #     $connection.Close()
    # }

    #TODO - figure out a way to check if all is well before deleting old db???