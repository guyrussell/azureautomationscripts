
    $connectionName = "AzureRunAsConnection"
       
    try 
    {
        # Get the connection "AzureRunAsConnection "
        $servicePrincipalConnection = Get-AutomationConnection -Name $connectionName
 
        "Login to Azure"
 
        Add-AzureRmAccount `
        -ServicePrincipal `
        -TenantId $servicePrincipalConnection.TenantId `
        -ApplicationId $servicePrincipalConnection.ApplicationId `
        -CertificateThumbprint $servicePrincipalConnection.CertificateThumbprint
    }
 
    catch 
    {
        if (!$servicePrincipalConnection) 
        {
            $ErrorMessage = "Connection $connectionName not found."
            throw $ErrorMessage
        }
        else 
        {
            Write-Error -Message $_.Exception
            throw $_.Exception
        }
    }

    $SendGridUsername =(Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "Send-Grid-User-Name").SecretValueText
    $SendGridPassword = ConvertTo-SecureString -String (Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "Send-Grid-Password").SecretValueText -AsPlainText -Force
    $credential = New-Object System.Management.Automation.PSCredential $SendGridUsername, $SendGridPassword
    $SMTPServer = "smtp.sendgrid.net"
    $EmailFrom = "automated@vuemed.com"
    $EmailTo = "grussell@vuemed.com"

    #send me the start time
    $date = ((Get-Date).AddHours(-7)).ToString("yyyy-MM-dd-HH-mm")
    $Subject = "scrub-and-shrink demo start"
    $Body = "<p>scrub & shrink demo process started</p><p>$date Pacific Daylight Time</p>"
    Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml

    $dbUsername = (Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "Demo-Database-UserName").SecretValueText
    $dbPassword = (Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "Demo-Database-Password").SecretValueText
    $securePassword = ConvertTo-SecureString -String $dbPassword -AsPlainText -Force

    $scrubAndShrinkOk = $false
    
    #################### setup ##################################
    Try
    {
        # $DatabaseName = "ProdCopy"
        
        # $ServerName = "vuemeddb"
        
        # $ResourceGroupName = "VueMed_Demo_Resource_Group"

        # $StorageKeytype = "StorageAccessKey"
        
        # $StorageKey = (Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM"`
        # –Name "databasebackuphot-storage-access-key").SecretValueText

        # $context = New-AzureStorageContext -StorageAccountName 'databasebackuphot' -StorageAccountKey $StorageKey

        # $storageUri = 'https://databasebackuphot.blob.core.windows.net/backups/'
       
        $scrubAndShrinkOk = $true
    }
    Catch
    {
        Write-Output $error
        $Subject = "failure in scrub-and-shrink demo"
        $Body = "<p>Broke trying to set stuff up</p><p>$error</p>"
        Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml

        $restoreProdComplete = $false
    }


    ######################### start scrubbing db ################################################
    
    $connectionString = (Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "Demo-Database-ProdCopy-ConnectionString").SecretValueText
    $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
    $command = new-object system.data.sqlclient.sqlcommand("",$connection)
    $connection.Open()

    If ($scrubAndShrinkOk -eq $true)
    {
        Try
        {
            $command.CommandText = 'delete RFIDSyncRecord'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
        }
        Catch
        {
            Write-Output $error
            $scrubAndShrinkOk = $false

            $Subject = "failure in scrub-and-shrink demo"
            $Body = "<p>Broke in very first truncate/delete section</p><p>$error</p>"
            Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
        }
    }


# ## Start-Sleep -s 10


    If ($scrubAndShrinkOk -eq $true)
    {
        Try
        {
            $command.CommandText = 'update Site set Name = ''O1'' where siteID = 3 '# '  -- old MGH
            $command.ExecuteNonQuery()
            $command.CommandText = 'update Site set Name = ''Hospital M'' where siteID = 4  '#  -- MGH
            $command.ExecuteNonQuery()
            $command.CommandText = 'update Site set Name = ''Hospital N'' where siteID = 5 '#'  -- NWH
            $command.ExecuteNonQuery()
            $command.CommandText = 'update Site set Name = ''Hospital W'' where siteID = 6 '#' -- WHC
            $command.ExecuteNonQuery()
            $command.CommandText = 'update Site set Name = ''Hospital B'' where siteID = 7 '#' -- Baystate
            $command.ExecuteNonQuery()
            $command.CommandText = 'update Site set Name = ''Hospital S'' where siteID = 8 '#'  -- SouthWest
            $command.ExecuteNonQuery()
            $command.CommandText = 'update Site set Name = ''O2'' where siteID = 9 '#'  -- test MGH
            $command.ExecuteNonQuery()
            $command.CommandText = 'update Site set Name = ''W2'' where siteID = 10 '#'  -- WHC OR Pilot
            $command.ExecuteNonQuery()
            $command.CommandText = 'update Site set Name = ''Springfield General Hospital'' where siteID = 11 '#'  -- Liverpool'
            $command.ExecuteNonQuery()
            $command.CommandText = 'update Site set Name = ''Demo RFID Site'' where siteID = 42 '#'  -- Loma Linda
            $command.ExecuteNonQuery()


            # need to clear out the remaining names?
        }
        Catch
        {
            Write-Output $error
            $scrubAndShrinkOk = $false 
            $Subject = "failure in scrub-and-shrink demo"
            $Body = "<p>Broke by Update Site section</p><p>$error</p>"
            Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
        }
    }

    If ($scrubAndShrinkOk -eq $true)
    {
        Try
        {
            #drop all sync external tables - don't need them here
            $command.CommandText = 'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''EXT_VuemedSync_V4_CartOperation'')) BEGIN Drop External Table EXT_VuemedSync_V4_CartOperation END'
            $command.ExecuteNonQuery()
            $command.CommandText = 'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''EXT_VuemedSync_V4_Incoming'')) BEGIN Drop External Table EXT_VuemedSync_V4_Incoming END'
            $command.ExecuteNonQuery()
            $command.CommandText = 'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''EXT_VuemedSync_V4_IncomingBatch'')) BEGIN Drop External Table EXT_VuemedSync_V4_IncomingBatch END'
            $command.ExecuteNonQuery()
            $command.CommandText = 'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''EXT_VuemedSync_V4_NewProduct'')) BEGIN Drop External Table EXT_VuemedSync_V4_NewProduct END'
            $command.ExecuteNonQuery()
            $command.CommandText = 'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''EXT_VuemedSync_V4_Outgoing'')) BEGIN Drop External Table EXT_VuemedSync_V4_Outgoing END'
            $command.ExecuteNonQuery()
            $command.CommandText = 'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''EXT_VuemedSync_V4_Procedure'')) BEGIN Drop External Table EXT_VuemedSync_V4_Procedure END'
            $command.ExecuteNonQuery()
            $command.CommandText = 'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''EXT_VuemedSync_V4_ProcedureAttribute'')) BEGIN Drop External Table EXT_VuemedSync_V4_ProcedureAttribute END'
            $command.ExecuteNonQuery()
            $command.CommandText = 'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''EXT_VuemedSync_V4_ProcedureStaff'')) BEGIN Drop External Table EXT_VuemedSync_V4_ProcedureStaff END'
            $command.ExecuteNonQuery()
            $command.CommandText = 'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''EXT_VuemedSync_V4_Remove'')) BEGIN Drop External Table EXT_VuemedSync_V4_Remove END'
            $command.ExecuteNonQuery()
            $command.CommandText = 'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''EXT_VuemedSync_V4_RemoveBatch'')) BEGIN Drop External Table EXT_VuemedSync_V4_RemoveBatch END'
            $command.ExecuteNonQuery()
            $command.CommandText = 'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''EXT_VuemedSync_V4_Rescan'')) BEGIN Drop External Table EXT_VuemedSync_V4_Rescan END'
            $command.ExecuteNonQuery()
            $command.CommandText = 'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''EXT_VuemedSync_V4_RescanBatch'')) BEGIN Drop External Table EXT_VuemedSync_V4_RescanBatch END'
            $command.ExecuteNonQuery()
            $command.CommandText = 'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''EXT_VuemedSync_V4_ScanCapture'')) BEGIN Drop External Table EXT_VuemedSync_V4_ScanCapture END'
            $command.ExecuteNonQuery()
            $command.CommandText = 'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''EXT_VuemedSync_V4_Transfer'')) BEGIN Drop External Table EXT_VuemedSync_V4_Transfer END'
            $command.ExecuteNonQuery()
            $command.CommandText = 'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''EXT_VuemedSync_V4_TransferBatch'')) BEGIN Drop External Table EXT_VuemedSync_V4_TransferBatch END'
            $command.ExecuteNonQuery()
            $command.CommandText = 'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''EXT_VuemedSync_V4_UserLog'')) BEGIN Drop External Table EXT_VuemedSync_V4_UserLog END'
            $command.ExecuteNonQuery()


            $command.CommandText = 'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''ColumnDemo'')) BEGIN Drop External Table ColumnDemo END'
            $command.ExecuteNonQuery()

            $command.CommandText = 'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''ColumnSetDemo'')) BEGIN Drop External Table ColumnSetDemo END'
            $command.ExecuteNonQuery()

            $command.CommandText = 'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''ReportSetHierarchyDemo'')) BEGIN Drop External Table ReportSetHierarchyDemo END'
            $command.ExecuteNonQuery()

            $command.CommandText = 'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''ReportSetDemo'')) BEGIN Drop External Table ReportSetDemo END'
            $command.ExecuteNonQuery()

            $command.CommandText = 'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''UserDemo'')) BEGIN Drop External Table UserDemo END'
            $command.ExecuteNonQuery()
 
            ## make sure these use the same names as the azure migration stuff!!!!!
            $command.CommandText = 'IF (EXISTS (SELECT * FROM sys.external_data_sources Where [Name] = ''ExternalDataSource'' )) BEGIN Drop EXTERNAL DATA SOURCE ExternalDataSource END'
            $command.ExecuteNonQuery()
                        
            $command.CommandText = 'if exists (select * from sys.database_scoped_credentials where [Name] = ''DatabaseScopedCredential'') Begin DROP DATABASE SCOPED CREDENTIAL DatabaseScopedCredential End'
            $command.ExecuteNonQuery()

            $command.CommandText = 'if exists (SELECT * FROM sys.symmetric_keys AS SK WHERE SK.name = ''##MS_DatabaseMasterKey##'') Begin Drop Master Key End'
            $command.ExecuteNonQuery()
            
            $command.CommandText = "CREATE MASTER KEY ENCRYPTION BY PASSWORD = '$dbPassword'"
            $command.ExecuteNonQuery()
            
            $command.CommandText = "CREATE DATABASE SCOPED CREDENTIAL DatabaseScopedCredential WITH IDENTITY = '$dbUsername', SECRET = '$dbPassword'"
            $command.ExecuteNonQuery()
                        
            $command.CommandText = 'CREATE EXTERNAL DATA SOURCE ExternalDataSource WITH (TYPE = RDBMS, LOCATION = ''vuemeddb.database.windows.net'', DATABASE_NAME = ''VueMedDemo'', CREDENTIAL = DatabaseScopedCredential );'
            $command.ExecuteNonQuery()
                            
            $command.CommandText = 'CREATE External TABLE [UserDemo] ( [UserID][int] NOT NULL, [FirstName] [nvarchar] (50) NULL, [LastName] [nvarchar] (50) NULL, [FullName] [nvarchar] (100) NULL, [Email] [nvarchar] (50) NULL, [UserName] [nvarchar] (50) NOT NULL, [PasswordEncrypted] [nvarchar] (150) NOT NULL, [Deleted] [bit] NOT NULL, [RoleID] [int] NULL, [Phone] [nvarchar] (20) NULL, [PhoneExt] [nvarchar] (10) NULL, [DateCreated] [datetime] NOT NULL, [DateModified] [datetime] NULL, [LastModifiedBy] [varchar] (50) NULL, [SiteID] [int] NULL, [ResetPassword] [bit] NOT NULL, [PIN] [int] NULL, [FiltersDefaultSetting] [nvarchar](max) NULL, [RescanAccess] [bit] NOT NULL, [ManufacturerID] [int] NULL, [LockedOut] [bit] NOT NULL, [FailedLoginAttemptCount] [int] NOT NULL, [LastPasswordChangeDate] [datetime] NULL, [ChangePasswordOnNextLogon] [bit] NOT NULL) WITH(DATA_SOURCE = ExternalDataSource, SCHEMA_NAME = ''dbo'', OBJECT_NAME = ''User'');'
            $command.ExecuteNonQuery()
            

            #create a temp table - insert into it users from the demo db that are not in the prod db
            $command.CommandText = 'Select * Into TempUserTable from (Select * From UserDemo where Username not in (select Username from [User])) as tablename '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
        }
        Catch
        {
            Write-Output $error
            $scrubAndShrinkOk = $false
            $Subject = "failure in scrub-and-shrink demo"
            $Body = "<p>Broke by external table section</p><p>$error</p>"
            Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
        }
    }

#Start-Sleep -s 10

    If ($scrubAndShrinkOk -eq $true)
    {
        Try
        {
#           # need to delete all users - going to need to delete all pesky constraint stuff first
            $command.CommandText = 'delete UserPermission'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete ManufacturerFilter'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete SiteRole'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'Alter Table ProductOverrideBackup Drop Constraint FK_ProductOverrideBackup_User'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'update ProductOverrideBackup Set UserID = -1 Where UserID is not null'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'Alter Table ProductOverride Drop Constraint FK_ProductOverride_User'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'update ProductOverride Set UserID = -1 Where UserID is not null'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'Alter Table BulkZeroLog Drop Constraint FK_BulkZeroLog_User'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'update BulkZeroLog Set UserID = -1 Where UserID is not null'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete RescanPoolProduct'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete RescanEventLog'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete Rescan'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete RescanBatch'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete RescanPool'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete DupeInfo'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete RequisitionItemResolutionState'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete UserMemoryReadLog'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete Incoming where roomid in (select roomid from room where siteid not in (7, 11, 24, 42))'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'Alter Table [Transfer] Drop Constraint FK_Transfer_User'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'update [Transfer] Set UserID = -1 Where UserID is not null'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete CartEvent'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete ReportSetHierarchy'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete RecentPassword'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete AdminLog'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete OrderEventLog '# this was all of them Where OrderID in (Select OrderID From [Order] Where UserID is not null)'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete OrderProduct '#Where OrderID in (Select OrderID From [Order] Where UserID is not null)'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete [Order] '#Where UserID is not null'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete RetiredTagLog'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete AdjustmentLog'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete HPandHRPValuesHistory'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete UserToken'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete EPCRetireLog'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete UnknownItemMasterItem'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete DepartmentProductHistory'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete ProcedureAdjustmentLog'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete PLParamSetting'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete RescanEventLog'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete ColumnSetting'### where userid > 1
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete [user]' 
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
        }
        Catch
        {   
            Write-Output $error
            $scrubAndShrinkOk = $false
            $Subject = "failure in scrub-and-shrink demo"
            $Body = "<p>Broke by user delete section</p><p>$error</p>"
            Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
        }
    }

# #Start-Sleep -s 10 

    If ($scrubAndShrinkOk -eq $true)
    {
        Try
        {
            # #turn on identity insert"
            $command.CommandText = 'set identity_insert [User] On'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'insert into [User] ( [UserID], [FirstName], [LastName], [FullName], [Email], [UserName], [PasswordEncrypted], [Deleted], [RoleID], [Phone], [PhoneExt], [DateCreated], [DateModified], [LastModifiedBy], [SiteID], [ResetPassword], [PIN], [FiltersDefaultSetting], [RescanAccess], [ManufacturerID], [LockedOut], [FailedLoginAttemptCount], [LastPasswordChangeDate], [ChangePasswordOnNextLogon]) Select [UserID], [FirstName], [LastName], [FullName], [Email], [UserName], [PasswordEncrypted], [Deleted], [RoleID], [Phone], [PhoneExt], [DateCreated], [DateModified], [LastModifiedBy], [SiteID], [ResetPassword], [PIN], [FiltersDefaultSetting], [RescanAccess], [ManufacturerID], [LockedOut], [FailedLoginAttemptCount], [LastPasswordChangeDate], [ChangePasswordOnNextLogon] From TempUserTable '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

             # need to add default user back in
            $command.CommandText = 'insert into [user] (Userid, UserName, PasswordEncrypted, DateCreated) values (0, ''default'', ''GLklWy4yAPvpP2XUl0Y3WW6BQUQRp0CumeO0'', GETDATE()) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            #turn off identity insert"
            $command.CommandText = 'set identity_insert [User] Off'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'Drop Table TempUserTable'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

        }
        Catch
        {
            Write-Output $error
            $scrubAndShrinkOk = $false
            $Subject = "failure in scrub-and-shrink demo"
            $Body = "<p>Broke by identity insert section</p><p>$error</p>"
            Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
        }
    }

Start-Sleep -s 10

    If ($scrubAndShrinkOk -eq $true)
    {
        Try
        {
            $command.CommandText = 'update HistoricalPrice set Price = Price * (1.4 + cast (abs(cast(cast(newid() as binary(4)) as int)) % 21 as float)/100.0) where siteid in (7, 11, 42) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
                                                                       
            $command.CommandText = 'update HistoricalReimbursementPrice set ReimbursementPrice = ReimbursementPrice * (1.4 + cast (abs(cast(cast(newid() as binary(4)) as int)) % 21 as float)/100.0) where siteid = 11 '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()


            $command.CommandText = 'DROP TABLE IF EXISTS PriceTable'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()


	        $command.CommandText = 'Create Table PriceTable (PriceTableID int, SiteID int, DepartmentID int, ProductID int, [Date] DateTime, Price decimal(18,2))'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()


	        $command.CommandText = 'Insert Into PriceTable (PriceTableID, SiteID, DepartmentID, ProductID, [Date]) Select Max(HistoricalPriceID), Siteid, departmentid, productid, ValidTo from HistoricalPrice group by Siteid, departmentid, productid, ValidTo'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

	
	        $command.CommandText = 'Update PriceTable set [Date] = ''3000-01-01 00:00:01.000'' Where [Date] is null'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()


	        $command.CommandText = 'Update PriceTable Set Price = (Select Price from HistoricalPrice Where HistoricalPriceID = PriceTableID)'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

	
	        $command.CommandText = 'update HPVMaterialized set price = ( select price from PriceTable where HPVMaterialized.SiteID = PriceTable.SiteID and HPVMaterialized.DepartmentID = PriceTable.DepartmentID and HPVMaterialized.ProductID = PriceTable.ProductID and HPVMaterialized.ValidTo = DATEADD(second, -1,  PriceTable.[Date]))'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()


	        $command.CommandText = 'DROP TABLE IF EXISTS PriceTable'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()


            $command.CommandText = 'update [procedure] set name = [dbo].[removeStringCharacters] (name, ''abcdefghijklmnopqrstuvwxyz,-.'') from [procedure] p where roomid in (select roomid from room where siteid = 11) and ((patindex(''%[a-z][a-z][a-z]%'', name) > 0) and (not (name like ''%test%''))) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'update product set primaryfeature = ''custom pack'' where primaryfeature like ''%baystate%'' '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'update product set secondaryfeature = ''custom pack'' where secondaryfeature like ''%baystate%'' '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'update DepartmentProductData set ReorderLevel  = ParLevel + abs(cast(cast(newid() as binary(4)) as int)) %6 where SiteID = 7 and parlevel <> 0 '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'update site set OrderTypeID = 1 where SiteID in (7,11) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
        }
        Catch
        {
            Write-Output $error
            $scrubAndShrinkOk = $false
            $Subject = "failure in scrub-and-shrink demo"
            $Body = "<p>Broke by update historical price section</p><p>$error</p>"
            Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
        }
    }

Start-Sleep -s 10

    If ($scrubAndShrinkOk -eq $true)
    {
        Try
        {
            $command.CommandText = "delete from ReportSet "
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = "delete from [ColumnSet] "
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = "delete from reportsethierarchy "
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = "delete from [column] "
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()


            $command.CommandText = 'CREATE External TABLE [dbo].[ColumnDemo] ( [ColumnID][int] NOT NULL, [Name] [varchar] (50) NOT NULL, [SortOn] [bit] NOT NULL, [SortOrder] [int] NOT NULL, [ColumnSetID] [int] NOT NULL, [Index] [int] NULL) WITH (DATA_SOURCE = ExternalDataSource, SCHEMA_NAME = ''dbo'',  OBJECT_NAME = ''Column'')'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

#Start-Sleep -s 10

            $command.CommandText = 'CREATE External TABLE [dbo].[ColumnSetDemo] ( [ColumnSetID][int] NOT NULL, [ReportId] [varchar] (50) NOT NULL, [UserId] [int] NOT NULL, [LabelName] [varchar] (200) NULL, [OutputType] [int] NOT NULL, [IsDefault] [bit] NOT NULL) WITH (DATA_SOURCE = ExternalDataSource, SCHEMA_NAME = ''dbo'',  OBJECT_NAME = ''ColumnSet'')'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

#Start-Sleep -s 10

            $command.CommandText = 'CREATE External TABLE[dbo].[ReportSetHierarchyDemo] ( [ReportSetHierarchyID][int] NOT NULL, [Name] [varchar] (50) NOT NULL, [ParentID] [int] NULL, [UserID] [int] NULL, [ChildOrder] [int] NULL) WITH (DATA_SOURCE = ExternalDataSource, SCHEMA_NAME = ''dbo'',  OBJECT_NAME = ''ReportSetHierarchy'')'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

#start-Sleep -s 10

            $command.CommandText = 'CREATE External TABLE [dbo].[ReportSetDemo] ( [ReportSetID][int] NOT NULL, [UserID] [int] NULL, [Title] [nvarchar] (200) NULL, [DateCreated] [datetime] NULL, [DateModified] [datetime] NULL, [LastModifiedBy] [varchar] (50) NULL, [Report] [varchar] (50) NULL, [FilterData] [nvarchar] (max) NULL, [IsSummary] [bit] NOT NULL, [Guid] [uniqueidentifier] NOT NULL, [ContainerID] [int] NULL, [ColumnSetID] [int] NULL, [ChildOrder] [int] NULL) WITH (DATA_SOURCE = ExternalDataSource, SCHEMA_NAME = ''dbo'',  OBJECT_NAME = ''ReportSet'')'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

#Start-Sleep -s 10

            $command.CommandText = 'set identity_insert [columnset] on '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'insert into columnset (ColumnSetID, ReportID, UserID, LabelName, OutputType, IsDefault) select ColumnSetID, ReportID, nu.UserID, LabelName, OutputType, IsDefault from columnsetDemo cs inner join [userDemo] ou on cs.UserID = ou.UserId inner join [User] nu on ou.UserName = nu.UserName order by cs.ColumnSetID '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'set identity_insert [columnset] off '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'set identity_insert [column] on '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'insert into [column] (ColumnID, Name, SortOn, SortOrder, ColumnSetID) select ColumnID, Name, SortOn, SortOrder, ColumnSetID from [columnDemo] where ColumnSetID in (select columnsetid from columnset) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'set identity_insert [column] off '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'set identity_insert reportsethierarchy on'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'insert into reportsethierarchy (ReportSetHierarchyID, Name, ParentID, UserID, ChildOrder) select ReportSetHierarchyID, Name, ParentID, nu.UserID, ChildOrder from reportsethierarchyDemo rsh inner join [userDemo] ou on rsh.UserID = ou.UserId inner join [User] nu on ou.UserName = nu.UserName'
            $command.ExecuteNonQuery()
            $command.CommandText = 'set identity_insert reportsethierarchy off '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'set identity_insert reportset on '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'insert into reportset (ReportSetID, UserID, Title, DateCreated, DateModified, LastModifiedBy, Report, FilterData, IsSummary, Guid, ContainerID, ColumnSetID, ChildOrder) select ReportSetID, nu.UserID, Title, rs.DateCreated, rs.DateModified, rs.LastModifiedBy, Report, FilterData, IsSummary, Guid, ContainerID, ColumnSetID, ChildOrder from reportsetDemo rs inner join [userDemo] ou on rs.UserID = ou.UserId inner join [User] nu on ou.UserName = nu.UserName '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'set identity_insert reportset off '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

           
            $command.CommandText = 'drop external table [columnDemo]'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'drop external table [columnsetDemo]'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'drop external table [reportsethierarchyDemo]'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'drop external table [reportsetDemo]'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            
            $command.CommandText = 'drop external table [UserDemo]'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
        }
        Catch
        {
            Write-Output $error
            $scrubAndShrinkOk = $false
            $Subject = "failure in scrub-and-shrink demo"
            $Body = "<p>Broke by identity insert reports and columns section</p><p>$error</p>"
            Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
        }
    }

#Start-Sleep -s 10

    If ($scrubAndShrinkOk -eq $true)
    {
       Try
       {
            $command.CommandText = 'update machine set description = ''machine'' + cast (machineid as varchar(5)) where roomid in (select roomid from room where siteid = 7) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'update DepartmentProductData set ReorderNumber = ''R'' + cast(ProductID as varchar(8)) where SiteID in (7, 42) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'update Site set Name = ''RFID Demo Cardio'' where SiteID = 42 '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'update [user] set SiteID = 42 where UserName = ''demorfid'' '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete from remove where roomid not in (select roomid from room where siteid in (7, 11, 24, 42)) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete from [productoverride] where roomid not in (select roomid from room where siteid in (7, 11, 24, 42)) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete from outgoing where roomid in (select roomid from room where siteid in (4)) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
        }
        Catch
        {
            Write-Output $error
            $scrubAndShrinkOk = $false 
            $Subject = "failure in scrub-and-shrink demo"
            $Body = "<p>Broke by update machine section</p><p>$error</p>"
            Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
        }
    }


    If ($scrubAndShrinkOk -eq $true)
    {
        Try
        {
            $command.CommandText = 'delete from DepartmentProductData where siteID not in (7, 11, 24, 42) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            #this one takes a very, very long time 11 mins on the server
            $command.CommandText = 'delete from outgoing where roomid in (select roomid from room where siteid not in (7, 11, 24, 42)) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
        }
        Catch
        {
            Write-Output $error
            $scrubAndShrinkOk = $false
            $Subject = "failure in scrub-and-shrink demo"
            $Body = "<p>Broke by delete from outgoing section</p><p>$error</p>"
            Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
        }
    }

    If ($scrubAndShrinkOk -eq $true)
    {
        Try
        {
            $command.CommandText = 'delete from OriginalProductData where ProcedureID not in (select ProcedureID from[Procedure] where roomid in (select roomid from room where siteid in (7, 11, 24, 42)))'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete from ProcedureAttribute where ProcedureID not in (select ProcedureID from[Procedure] where roomid in (select roomid from room where siteid in (7, 11, 24, 42)))'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete from [procedureStaff] where procedureid in (select distinct p.ProcedureID from[procedure] p inner join room r on p.RoomID = r.RoomID where r.siteID not in (7, 11, 24, 42))'
            $command.CommandTimeout = 3600 
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete from [procedure] where roomid not in (select roomid from room where siteid in (7, 11, 24, 42))'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete from physiciandepartment where siteID not in (7, 11, 24, 42)'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete from physician where siteID not in (7, 11, 24, 42)'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()


            $command.CommandText = 'CREATE External TABLE [dbo].[EXT_NamesStaff] ( [NamesStaffID] [int] NULL, [StaffID] [int] NULL, [Name] [nvarchar](200) NULL) WITH (DATA_SOURCE = ExternalDataSource, SCHEMA_NAME = ''dbo'',  OBJECT_NAME = ''NamesStaff'')'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'CREATE External TABLE [dbo].[EXT_NamesPhysician] ( [NamesPhysicianID] [int] NULL, [PhysicianID] [int] NULL, [Name] [nvarchar](200) NULL) WITH (DATA_SOURCE = ExternalDataSource, SCHEMA_NAME = ''dbo'',  OBJECT_NAME = ''NamesPhysician'')'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            # #make these tables in this database as well so that it carries over to the next time we do this
            $command.CommandText = 'Select NamesStaffID, StaffID, [Name] Into NamesStaff From [EXT_NamesStaff]'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'Select NamesPhysicianID, PhysicianID, [Name] Into NamesPhysician From [EXT_NamesPhysician]'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'update Staff set [name] = (select [name] from NamesStaff where NamesStaff.StaffID = Staff.StaffID)'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'update Physician set [name] = (select [name] from NamesPhysician where NamesPhysician.PhysicianID = Physician.PhysicianID)'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'drop external table [EXT_NamesStaff]'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'drop external table [EXT_NamesPhysician]'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()


            $command.CommandText = 'delete from techdepartment where siteID not in (7, 11, 24, 42)'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete from tech where siteID not in (7, 11, 24, 42)'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'drop EXTERNAL DATA SOURCE ExternalDataSource '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
        }
        Catch
        {
            Write-Output $error
            $scrubAndShrinkOk = $false
            $Subject = "failure in scrub-and-shrink demo"
            $Body = "<p>Broke by update physician and staff section</p><p>$error</p>"
            Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
        }
    }

    If ($scrubAndShrinkOk -eq $true)
    {
        Try
        {
            $command.CommandText = 'delete from SiteProductLot '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete from SiteProduct where siteID not in (7, 11, 24, 42) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete from Barcode '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            
            #gotta do this first - pesky referential integrity stuff

            $command.CommandText = "IF OBJECT_ID('tempdb..#bob') IS NOT NULL DROP TABLE #bob "
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'select distinct lotid into #bob from ( select lotid from outgoing union all select lotid from incoming union all select lotid from originalproductdata union all select lotid from productoverride union all select lotid from [transfer] union all select lotid from [remove] union all select LotID from EPCLot ) q1 '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete from ProductOverrideBackup where lotid not in (select LotID from #bob) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = "IF OBJECT_ID('tempdb..#bob') IS NOT NULL DROP TABLE #bob "
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'delete from lot where lotid not in (select distinct lotid from outgoing union all select lotid from incoming union all select lotid from originalproductdata union all select lotid from productoverride union all select lotid from [transfer] union all select lotid from [remove] union all select LotID from EPCLot) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()

            $command.CommandText = 'update manufacturer set IsActive = 0 where ManufacturerID not in (select ManufacturerID from product)'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
        }
        Catch
        {
            Write-Output $error
            $scrubAndShrinkOk = $false 
            $Subject = "failure in scrub-and-shrink demo"
            $Body = "<p>Broke just before alter index section</p><p>$error</p>"
            Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
        }

    }

    $connection.Close()

 # send complete or not complete   

If ($scrubAndShrinkOk -eq $true)
{
    $date = ((Get-Date).AddHours(-7)).ToString("yyyy-MM-dd-HH-mm")
     $Subject = "scrub and shrink complete!"
     $Body = "<p>everything okey doke</p><p>At $date</p>"
     Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml

    #call the next one
    Invoke-RestMethod -Method Post -Uri 'https://s1events.azure-automation.net/webhooks?token=B9J9I9GiwnOX2bEQscgIk8E1qpww8rtFGZadurr8DM8%3d'

}
else

{
    $date = ((Get-Date).AddHours(-7)).ToString("yyyy-MM-dd-HH-mm")
     $Subject = "scrub and shrink failed!"
     $Body = "<p>everything NOT okey doke</p><p>At $date</p>"
     Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
}
