

    $connectionName = "AzureRunAsConnection"
       
    try 
    {
        # Get the connection "AzureRunAsConnection "
        $servicePrincipalConnection = Get-AutomationConnection -Name $connectionName
 
        "Login to Azure"
 
        Add-AzureRmAccount `
        -ServicePrincipal `
        -TenantId $servicePrincipalConnection.TenantId `
        -ApplicationId $servicePrincipalConnection.ApplicationId `
        -CertificateThumbprint $servicePrincipalConnection.CertificateThumbprint
    }
 
    catch 
    {
        if (!$servicePrincipalConnection) 
        {
            $ErrorMessage = "Connection $connectionName not found."
            throw $ErrorMessage
        }
        else 
        {
            Write-Error -Message $_.Exception
            throw $_.Exception
        }
    }


    $SendGridUsername =(Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "Send-Grid-User-Name").SecretValueText
    $SendGridPassword = ConvertTo-SecureString -String (Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "Send-Grid-Password").SecretValueText -AsPlainText -Force
    $credential = New-Object System.Management.Automation.PSCredential $SendGridUsername, $SendGridPassword
    $SMTPServer = "smtp.sendgrid.net"
    $EmailFrom = "automated@vuemed.com"
    $EmailTo = "grussell@vuemed.com"

    #send me the start time
    $date = ((Get-Date).AddHours(-7)).ToString("yyyy-MM-dd-HH-mm")
    $Subject = "rename databases started"
    $Body = "<p>rename databases</p><p>rename prod copy to demo</p><p>$date Pacific Daylight Time</p>"
    Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml

    
    $connectionString = (Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "Demo-Database-Master-ConnectionString").SecretValueText
    $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
    $command = new-object system.data.sqlclient.sqlcommand("",$connection)
    $connection.Open()

    Try
    {
        $date = ((Get-Date).AddHours(-7)).ToString("yyyy_MM_dd_HH_mm")
        $newName = "VueMedDemo_Stale"
        #$command.CommandText = "ALTER DATABASE VueMedDemo MODIFY Name = $newName"
        $command.CommandText = "Drop DATABASE [VueMedDemo]"
        $command.CommandTimeout = 3600
        $command.ExecuteNonQuery()

        Start-Sleep -s 300

        $command.CommandText = "ALTER DATABASE ProdCopy MODIFY Name = VueMedDemo"
        $command.CommandTimeout = 3600
        $command.ExecuteNonQuery()

        $connection.Close()
    
        Start-Sleep -s 900 

        $ServerName = "vuemeddb"
        $DatabaseName = "ProdCopy"
        $ResourceGroupName = "VueMed_Demo_Resource_Group"

    
        # resize old demo to lowest size
        #Set-AzureRmSqlDatabase -ResourceGroupName $ResourceGroupName -ServerName $ServerName -DatabaseName $newName -Edition "Standard" -RequestedServiceObjectiveName "S0"
        # resize new demo to S1 until you hear from management
        Set-AzureRmSqlDatabase -ResourceGroupName $ResourceGroupName -ServerName $ServerName -DatabaseName "VueMedDemo" -Edition "Standard" -RequestedServiceObjectiveName "S1"
        
        $date = ((Get-Date).AddHours(-7)).ToString("yyyy-MM-dd-HH-mm")
        $Subject = "rename databases complete"
        $Body = "<p>everything okey doke</p><p>At $date</p>"
        Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
    }
    Catch
    {
        $date = ((Get-Date).AddHours(-7)).ToString("yyyy-MM-dd-HH-mm")
        $Subject = "rename databases failed!"
        $Body = "<p>everything NOT okey doke</p><p>At $date</p><p>$error</p>"
        Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
    }
    Finally
    {
        $connection.Close()
    }

    #TODO - figure out a way to check if all is well before deleting old db???
