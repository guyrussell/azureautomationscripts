

      $connectionName = "AzureRunAsConnection"
       
    try 
    {
        # Get the connection "AzureRunAsConnection "
        $servicePrincipalConnection = Get-AutomationConnection -Name $connectionName
 
        "Login to Azure"
 
        Add-AzureRmAccount `
        -ServicePrincipal `
        -TenantId $servicePrincipalConnection.TenantId `
        -ApplicationId $servicePrincipalConnection.ApplicationId `
        -CertificateThumbprint $servicePrincipalConnection.CertificateThumbprint
    }
 
    catch 
    {
        if (!$servicePrincipalConnection) 
        {
            $ErrorMessage = "Connection $connectionName not found."
            throw $ErrorMessage
        }
        else 
        {
            Write-Error -Message $_.Exception
            throw $_.Exception
        }
    }

    $SendGridUsername =(Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "Send-Grid-User-Name").SecretValueText
    $SendGridPassword = ConvertTo-SecureString -String (Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "Send-Grid-Password").SecretValueText -AsPlainText -Force
    $credential = New-Object System.Management.Automation.PSCredential $SendGridUsername, $SendGridPassword
    $SMTPServer = "smtp.sendgrid.net"
    $EmailFrom = "automated@vuemed.com"
    $EmailTo = "grussell@vuemed.com"

    #send me the start time
    $date = ((Get-Date).AddHours(-7)).ToString("yyyy-MM-dd-HH-mm")
    $Subject = "shrink-and-stats demo start"
    $Body = "<p>shrink-and-stats demo process started</p><p>$date Pacific Daylight Time</p>"
    Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml


    $dbUsername = (Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "Demo-Database-UserName").SecretValueText
    $dbPassword = (Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "Demo-Database-Password").SecretValueText
    $securePassword = ConvertTo-SecureString -String $dbPassword -AsPlainText -Force

    $scrubAndShrinkOk = $true

    Try
    {
        $connectionString = (Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "Demo-Database-ProdCopy-ConnectionString").SecretValueText
        $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
        $command = new-object system.data.sqlclient.sqlcommand("",$connection)#weird initializer
        $connection.Open()
    }
    Catch
    {
        Write-Output $error
        $scrubAndShrinkOk = $false
        $Subject = "error in shrink-and-stats demo"
        $Body = "<p>Broke trying to connect to db</p><p>$error</p>"
        Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
    }

    $date = ((Get-Date).AddHours(-7)).ToString("yyyy-MM-dd-HH-mm")
    Write-Output "start of external table drop + $date"

    # drop external tables
    If ($scrubAndShrinkOk -eq $true)
    {
        Try
        {
            $command.CommandText = "IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'EXT_VuemedSync_V4_CartOperation')) BEGIN Drop External Table EXT_VuemedSync_V4_CartOperation END `
                                    IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'EXT_VuemedSync_V4_Incoming')) BEGIN Drop External Table EXT_VuemedSync_V4_Incoming END `
                                    IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'EXT_VuemedSync_V4_IncomingBatch')) BEGIN Drop External Table EXT_VuemedSync_V4_IncomingBatch END `
                                    IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'EXT_VuemedSync_V4_NewProduct')) BEGIN Drop External Table EXT_VuemedSync_V4_NewProduct END `
                                    IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'EXT_VuemedSync_V4_Outgoing')) BEGIN Drop External Table EXT_VuemedSync_V4_Outgoing END `
                                    IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'EXT_VuemedSync_V4_Procedure')) BEGIN Drop External Table EXT_VuemedSync_V4_Procedure END `
                                    IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'EXT_VuemedSync_V4_ProcedureAttribute')) BEGIN Drop External Table EXT_VuemedSync_V4_ProcedureAttribute END `
                                    IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'EXT_VuemedSync_V4_ProcedureStaff')) BEGIN Drop External Table EXT_VuemedSync_V4_ProcedureStaff END `
                                    IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'EXT_VuemedSync_V4_Remove')) BEGIN Drop External Table EXT_VuemedSync_V4_Remove END `
                                    IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'EXT_VuemedSync_V4_RemoveBatch')) BEGIN Drop External Table EXT_VuemedSync_V4_RemoveBatch END `
                                    IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'EXT_VuemedSync_V4_Rescan')) BEGIN Drop External Table EXT_VuemedSync_V4_Rescan END `
                                    IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'EXT_VuemedSync_V4_RescanBatch')) BEGIN Drop External Table EXT_VuemedSync_V4_RescanBatch END `
                                    IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'EXT_VuemedSync_V4_ScanCapture')) BEGIN Drop External Table EXT_VuemedSync_V4_ScanCapture END `
                                    IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'EXT_VuemedSync_V4_Transfer')) BEGIN Drop External Table EXT_VuemedSync_V4_Transfer END `
                                    IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'EXT_VuemedSync_V4_TransferBatch')) BEGIN Drop External Table EXT_VuemedSync_V4_TransferBatch END `
                                    IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'EXT_VuemedSync_V4_UserLog')) BEGIN Drop External Table EXT_VuemedSync_V4_UserLog END "
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
        }
        Catch
        {
            Write-Output $error
            $Subject = "shrink-and-stats demo"
            $Body = "<p>Broke dropping external tables</p><p>$error</p>"
            Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
        }
    }

    $date = ((Get-Date).AddHours(-7)).ToString("yyyy-MM-dd-HH-mm")
    Write-Output "start of alter all table indexes + $date"

    If ($scrubAndShrinkOk -eq $true)
    {
        Try
        {
            #takes 13 min on ssms
            $command.CommandText = 'DECLARE @TableName VARCHAR(255) DECLARE @sql NVARCHAR(500) DECLARE @fillfactor INT SET @fillfactor = 97 DECLARE TableCursor CURSOR FOR SELECT OBJECT_SCHEMA_NAME([object_id]) +''.['' + name + '']'' AS TableName FROM sys.tables OPEN TableCursor FETCH NEXT FROM TableCursor INTO @TableName WHILE @@FETCH_STATUS = 0 BEGIN SET @sql = ''ALTER INDEX ALL ON '' + @TableName + '' REBUILD WITH (FILLFACTOR = '' + CONVERT(VARCHAR(3), @fillfactor) + '')'' select @sql EXEC(@sql) FETCH NEXT FROM TableCursor INTO @TableName END CLOSE TableCursor DEALLOCATE TableCursor '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
        }
        Catch
        {
            Write-Output $error
            $Subject = "shrink-and-stats demo"
            $Body = "<p>Broke trying constructed alter index statement</p><p>$error</p>"
            Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
        }
    

#Start-Sleep -s 10
        $date = ((Get-Date).AddHours(-7)).ToString("yyyy-MM-dd-HH-mm")
        Write-Output "start of alter view indexes + $date"

    
        Try
        {
            $command.CommandText = 'ALTER INDEX ALL ON OutgoingLotIdxView1 REBUILD WITH (FILLFACTOR = 95) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'ALTER INDEX ALL ON OutgoingLotIdxView2 REBUILD WITH (FILLFACTOR = 95) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'ALTER INDEX ALL ON OutgoingLotIdxView3 REBUILD WITH (FILLFACTOR = 95) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'ALTER INDEX ALL ON IncomingLotIdxView REBUILD WITH (FILLFACTOR = 95) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'ALTER INDEX ALL ON ProductOverrideLotIdxView REBUILD WITH (FILLFACTOR = 95) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'ALTER INDEX ALL ON RemoveItemIdxView REBUILD WITH (FILLFACTOR = 95) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'ALTER INDEX ALL ON IncomingItemIdxView REBUILD WITH (FILLFACTOR = 95) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'ALTER INDEX ALL ON OutgoingItemIdxView1 REBUILD WITH (FILLFACTOR = 95) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'ALTER INDEX ALL ON OutgoingItemIdxView2 REBUILD WITH (FILLFACTOR = 95) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'ALTER INDEX ALL ON OutgoingItemIdxView3 REBUILD WITH (FILLFACTOR = 95) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'ALTER INDEX ALL ON RemoveLotIdxView REBUILD WITH (FILLFACTOR = 95) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'ALTER INDEX ALL ON TransferToItemIdxView REBUILD WITH (FILLFACTOR = 95) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'ALTER INDEX ALL ON TransferFromItemIdxView REBUILD WITH (FILLFACTOR = 95) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'ALTER INDEX ALL ON TransferToLotIdxView REBUILD WITH (FILLFACTOR = 95) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'ALTER INDEX ALL ON ScanOutgoingLotIdxView1 REBUILD WITH (FILLFACTOR = 95) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'ALTER INDEX ALL ON ScanOutgoingLotIdxView2 REBUILD WITH (FILLFACTOR = 95) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'ALTER INDEX ALL ON ScanOutgoingLotIdxView3 REBUILD WITH (FILLFACTOR = 95) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'ALTER INDEX ALL ON ScanIncomingLotIdxView REBUILD WITH (FILLFACTOR = 95)' 
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'ALTER INDEX ALL ON ScanRemoveItemIdxView REBUILD WITH (FILLFACTOR = 95) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'ALTER INDEX ALL ON ScanIncomingItemIdxView REBUILD WITH (FILLFACTOR = 95)' 
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'ALTER INDEX ALL ON ScanOutgoingItemIdxView1 REBUILD WITH (FILLFACTOR = 95) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'ALTER INDEX ALL ON ScanOutgoingItemIdxView2 REBUILD WITH (FILLFACTOR = 95) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'ALTER INDEX ALL ON ScanOutgoingItemIdxView3 REBUILD WITH (FILLFACTOR = 95) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'ALTER INDEX ALL ON ScanRemoveLotIdxView REBUILD WITH (FILLFACTOR = 95) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'ALTER INDEX ALL ON ScanTransferToItemIdxView REBUILD WITH (FILLFACTOR = 95) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'ALTER INDEX ALL ON ScanTransferFromItemIdxView REBUILD WITH (FILLFACTOR = 95)' 
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
            $command.CommandText = 'ALTER INDEX ALL ON ScanTransferToLotIdxView REBUILD WITH (FILLFACTOR = 95) '
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
        }
        Catch
        {
            Write-Output $error
            # $scrubAndShrinkOk = $false
            $Subject = "shrink-and-stats demo"
            $Body = "<p>alter index section failed</p><p>$error</p>"
            Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
        }
    }
    
    $date = ((Get-Date).AddHours(-7)).ToString("yyyy-MM-dd-HH-mm")
    Write-Output "start of updatestats + $date"

#Start-Sleep -s 30

    If ($scrubAndShrinkOk -eq $true)
    {
        Try
        {
            $command.CommandText = "exec sp_updatestats"
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
        }
        Catch
        {
            Write-Output "update stats broke" $error
            $Subject = "shrink-and-stats demo"
            $Body = "<p>update stats broke</p><p>$error</p>"
            Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
        }
    }

    $date = ((Get-Date).AddHours(-7)).ToString("yyyy-MM-dd-HH-mm")
    Write-Output "start of shrinkdatabase + $date"

#Start-Sleep -s 30

    If ($scrubAndShrinkOk -eq $true)
    {
        # db seems to need a bit of time here - give it 30 seconds
        Start-Sleep -s 120

        Try
        {
            $command.CommandText = 'dbcc shrinkdatabase(ProdCopy, 2)'
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
        }
        Catch
        {
            Write-Output "didn't need shrinking" $error
            $Subject = "shrink-and-stats demo shrink"
            $Body = "<p>didn't need shrinking</p><p>$error</p>"
            Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
        }
        
Start-Sleep -s 120

        $date = ((Get-Date).AddHours(-7)).ToString("yyyy-MM-dd-HH-mm")
        Write-Output "start of DBCC CHECKDB + $date"

        Try
            {
                $command.CommandText = 'DBCC CHECKDB '
                $command.CommandTimeout = 3600;
                $command.ExecuteNonQuery()

                #call the next one - backup
                Invoke-RestMethod -Method Post -Uri 'https://s1events.azure-automation.net/webhooks?token=NyVGQGsvDQJl3m1OL7y8R4ww%2bgWSyGpTI%2bR4Y%2bNfLBY%3d'
            }
            Catch
            {
                Write-Output "dbcc checkdb found something" $error
                $Subject = "dbcc checkdb found something"
                $Body = "<p>dbcc checkdb error</p><p>$error</p>"
                Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
            }
    }

    $connection.Close()


     #send me the end time
    $date = ((Get-Date).AddHours(-7)).ToString("yyyy-MM-dd-HH-mm")
    $Subject = "shrink-and-stats demo completed"
    $Body = "<p>shrink-and-stats demo process completed</p><p>$date Pacific Daylight Time</p>"
    Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
