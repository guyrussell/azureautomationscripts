
    $settingsXml = Get-AutomationVariable -Name SettingsDemo
    [xml]$ConfigFile = $settingsXml

    $Conn = Get-AutomationConnection -Name AzureRunAsConnection
    Connect-AzureRmAccount -ServicePrincipal -Tenant $Conn.TenantID -ApplicationId $Conn.ApplicationID `
    -CertificateThumbprint $Conn.CertificateThumbprint
    
    $BaseStorageUri = $ConfigFile.Settings.Storage.baseStorageUriProduction
    $CopyDatabaseName = $ConfigFile.Settings.Database.CopyDatabaseName
    $DatabaseName = $ConfigFile.Settings.Database.DatabaseName
    $DemoDatabaseUserName = $ConfigFile.Settings.Database.DemoDatabaseUserName
    $DemoDatabasePassword = $ConfigFile.Settings.Database.DemoDatabasePassword
    $EmailFrom = $ConfigFile.Settings.Email.emailFromAutomatedBackup
    $EmailTo = $ConfigFile.Settings.Email.EmailToOps
    $SendGridPassword = ConvertTo-SecureString -String $ConfigFile.Settings.Email.SendGridUserPassword -AsPlainText -Force
    $SendGridUsername = $ConfigFile.Settings.Email.SendGridUserName
    $ServerName = $ConfigFile.Settings.Database.ServerName
    $SMTPServer = $ConfigFile.Settings.Email.SMTPServer
    $StorageKey = $ConfigFile.Settings.Storage.databasebackuphot_storage_access_key
    $ResourceGroupName = $ConfigFile.Settings.Database.ResourceGroupName

    $credential = New-Object System.Management.Automation.PSCredential $SendGridUsername, $SendGridPassword
    $securePassword = ConvertTo-SecureString -String $DemoDatabasePassword -AsPlainText -Force
    $context = New-AzureStorageContext -StorageAccountName 'databasebackuphot' -StorageAccountKey $StorageKey

    ## this is getting the latest bacpac of the production database it can find and restore it as ProdCopy
    #send me the start time
    $date = ((Get-Date).AddHours(-7)).ToString("yyyy-MM-dd-HH-mm")
    $Subject = "restore from bacpac"
    $Body = "<p>restore production from bacpac to ProdCopy started</p><p>$date Pacific Daylight Time</p>"
    Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml

    

    #################### restore a copy of production (from bacpac) called ProdCopy ##################################
    Try
    {
        $StorageKeytype = "StorageAccessKey"

        ############# change this when we start actually backing up to bacpacs????? ###############

        $blob = Get-AzureStorageBlob -Container 'backups-azure' -Context $context  -blob 'VueMed-*' | Sort CreationTime | Select -Last 1
        $blobName = $blob.Name
        
        $bacPacUri = $BaseStorageUri + $blobName

        $importRequest = New-AzureRmSqlDatabaseImport `
        -ResourceGroupName $ResourceGroupName `
        -ServerName $ServerName `
        -DatabaseName $CopyDatabaseName `
        -StorageKeytype $StorageKeytype `
        -StorageKey $StorageKey `
        -StorageUri $bacPacUri `
        -AdministratorLogin $DemoDatabaseUserName `
        -AdministratorLoginPassword $securePassword `
        -Edition Standard `
        -ServiceObjectiveName 'S6' `
        -DatabaseMaxSizeBytes 268435456000

        $date = ((Get-Date).AddHours(-7)).ToString("yyyy-MM-dd-HH-mm")
        $Subject = "ProdCopy restore request initiated"
        $Body = "<p>VueMedProd bacpac restore to ProdCopy operation has been initiated</p><p>It it's still in test, remember to resize in 3 hours!!</p><p>$date Pacific Daylight Time (UTC - 7)</p>"
        Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml

    }
    Catch
    {
        Write-Output $error
        $Subject = "failure restoring ProdCopy"
        $Body = "<p>Broke trying to restore VueMedProd to ProdCopy</p><p>$error</p>"
        Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
    }
