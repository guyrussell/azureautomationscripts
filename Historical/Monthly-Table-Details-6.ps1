 
 $connectionName = "AzureRunAsConnection"
       
    try 
    {
        # Get the connection "AzureRunAsConnection "
        $servicePrincipalConnection = Get-AutomationConnection -Name $connectionName
 
        "Login to Azure"
 
        Add-AzureRmAccount `
        -ServicePrincipal `
        -TenantId $servicePrincipalConnection.TenantId `
        -ApplicationId $servicePrincipalConnection.ApplicationId `
        -CertificateThumbprint $servicePrincipalConnection.CertificateThumbprint
    }
 
    catch 
    {
        if (!$servicePrincipalConnection) 
        {
            $ErrorMessage = "Connection $connectionName not found."
            throw $ErrorMessage
        }
        else 
        {
            Write-Error -Message $_.Exception
            throw $_.Exception
        }
    }

    $connectionString = (Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "vuemedhistoricalinventory-connection-string").SecretValueText
    $destdb = 'vuemedhistoricalinventory'

    $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
    $command = new-object system.data.sqlclient.sqlcommand("",$connection)
        
    try
    {
        $connection.Open()

            $query = "Exec DropTwoYearOldData "
                
            write-output "starting $query"

            $command.CommandTimeout = 3600
            $command.CommandText = $query
            $command.ExecuteNonQuery()
    }
    catch
    {
        write-output "failed $query !!!"
    }
    finally
    {
        $connection.Close() 
    }
   
    


