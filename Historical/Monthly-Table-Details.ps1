 
 
 $connectionName = "AzureRunAsConnection"
       
    try 
    {
        # Get the connection "AzureRunAsConnection "
        $servicePrincipalConnection = Get-AutomationConnection -Name $connectionName
 
        "Login to Azure"
 
        Add-AzureRmAccount `
        -ServicePrincipal `
        -TenantId $servicePrincipalConnection.TenantId `
        -ApplicationId $servicePrincipalConnection.ApplicationId `
        -CertificateThumbprint $servicePrincipalConnection.CertificateThumbprint
    }
 
    catch 
    {
        if (!$servicePrincipalConnection) 
        {
            $ErrorMessage = "Connection $connectionName not found."
            throw $ErrorMessage
        }
        else 
        {
            Write-Error -Message $_.Exception
            throw $_.Exception
        }
    }

    ## uncomment if we need to send email error message
    # $SendGridUsername =(Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "Send-Grid-User-Name").SecretValueText
    # $SendGridPassword = ConvertTo-SecureString -String (Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "Send-Grid-Password").SecretValueText -AsPlainText -Force
    # $credential = New-Object System.Management.Automation.PSCredential $SendGridUsername, $SendGridPassword
    # $SMTPServer = "smtp.sendgrid.net"
    # $EmailFrom = "automated-data-harvest@vuemed.com"
    # $EmailTo = "canderson@vuemed.com, grussell@vuemed.com"

    $connectionString = (Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "vuemedhistoricalinventory-connection-string").SecretValueText
    #$destdb = 'vuemedhistoricalinventory'

    $webhook = (Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "monthly-table-details-2-webhook").SecretValueText

    $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
    $command = new-object system.data.sqlclient.sqlcommand("",$connection)

    $date = Get-Date
    $y = $date.Year
    $m = $date.Month

    $theFirst = Get-Date -Date "$y-$m-01"

    $ToMonth = $theFirst.ToString("yyyy-MM-dd")

    $Depts =  (7,  2,  3,  4,  3,   7,  28,  36,  39,   2,  23,   2)
	$Sites = (15,  5,  5,  5,  6,  31,  34,  42,  42,  46,  26,  26)

    $connection.Open()

    try
    {
        for ($i = 0; $i -lt $Depts.Count; $i++)
        {
            $DepartmentID = $Depts[$i]
            $SiteID = $Sites[$i]

            $query = "Exec MonthlySiteDetailsUpdate @SiteID = $SiteID, @DepartmentID = $DepartmentID, @ToMonth = '$ToMonth'"
            $command.CommandText = $query
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
        }
    
        #call the next runbook - Monthly-Table-Details-2
        Invoke-RestMethod -Method Post -Uri $webhook
    }
    catch
    {
        write-output "$query failed !!!"
    }
    finally
    {
        $connection.Close() 
    }

    

    