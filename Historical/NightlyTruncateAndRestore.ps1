    
    #this should run at 4:00am or later to make sure it happened!

     $Conn = Get-AutomationConnection -Name AzureRunAsConnection
    Connect-AzureRmAccount -ServicePrincipal -Tenant $Conn.TenantID -ApplicationId $Conn.ApplicationID `
    -CertificateThumbprint $Conn.CertificateThumbprint

    
    $settingsXml = Get-AutomationVariable -Name SettingsHistoric
    [xml]$ConfigFile = $settingsXml

   
    $connectionStringRestored = $ConfigFile.Settings.Database.connectionStringRestored
    $connectionStringRestoredMaster = $ConfigFile.Settings.Database.connectionStringRestoredMaster
    $connectionStringHistoric = $ConfigFile.Settings.Database.connectionStringHistoric

    $connectionStringRestored
    $connectionStringHistoric

    # $BaseStorageUri = $ConfigFile.Settings.Storage.baseStorageUri
    # $DatabaseName = $ConfigFile.Settings.Database.reportingDatabaseName
    # $UserName = $ConfigFile.Settings.Database.vuemed_production_sql_server_username
    #$Password = $ConfigFile.Settings.Database.vuemed_production_sql_server_password
    $EmailFrom = $ConfigFile.Settings.Email.emailFromAutomatedBackup
    $EmailTo = $ConfigFile.Settings.Email.EmailToOps
    $SendGridPassword = ConvertTo-SecureString -String $ConfigFile.Settings.Email.SendGridUserPassword -AsPlainText -Force
    $SendGridUsername = $ConfigFile.Settings.Email.SendGridUserName
    #$ServerName = $ConfigFile.Settings.Database.historicServer
    $SMTPServer = $ConfigFile.Settings.Email.SMTPServer
    #$StorageKey = $ConfigFile.Settings.Storage.databasebackuphot_storage_access_key
    #$ResourceGroupName = $ConfigFile.Settings.Database.ResourceGroupName

    $credential = New-Object System.Management.Automation.PSCredential $SendGridUsername, $SendGridPassword
    #$securePassword = ConvertTo-SecureString -String $Password -AsPlainText -Force
    #$context = New-AzureStorageContext -StorageAccountName 'databasebackuphot' -StorageAccountKey $StorageKey

    $backupDateAsDate = (Get-Date).ToString("yyyy-MM-dd")
    $backupDateAsDate

    Try
    {
        #the newly restored vuemed db on test server
        
        $sqlConnectionSource = New-Object System.Data.SqlClient.SqlConnection
        $sqlConnectionSource.ConnectionString = $connectionStringRestored
        $sqlcmdSource = New-Object System.Data.SqlClient.SqlCommand
        $sqlcmdSource.Connection = $sqlConnectionSource

 
        $sqlConnectionDest = new-object System.Data.SqlClient.SQLConnection
        $sqlConnectionDest.ConnectionString = $connectionStringHistoric
        $sqlcmdDest = New-Object System.Data.SqlClient.SqlCommand
        $sqlcmdDest.Connection = $sqlConnectionDest

    
        $tableNames = 
        (
        '[Attribute]', 
        '[BulkZeroDetailLog]', 
        '[BulkZeroLog]', 
        '[Category]', 
	    '[ConsumeReason]',
        '[Department]', 
        '[DepartmentProductData]', 
        '[HistoricalPrice]', 
        '[Incoming]', 
        '[IncomingBatch]', 
        '[Lot]', 
        '[Manufacturer]',
        '[Outgoing]', 
        '[Procedure]', 
        '[ProcedureAttribute]', 
        '[ProcedureStaff]', 
        '[Product]', 
        '[ProductType]',
        '[Reason]', 
        '[Remove]', 
        '[RemoveBatch]', 
        '[Room]', 
        '[ServiceLine]',
        '[Site]', 
        '[Staff]', 
        '[StaffDepartment]',
        '[StaffLabel]', 
        '[StaffType]', 
        '[Transfer]', 
        '[TransferBatch]'
         )
   

        $sqlConnectionSource.Open()
        $sqlConnectionDest.Open()

        foreach ($table in $tableNames)
        {
            try
            {
                #delete existing data from destination table
                $queryDest = "Truncate Table $table"
                write-output $queryDest

                $sqlcmdDest.CommandText = $queryDest
                $sqlcmdDest.ExecuteNonQuery()
                write-output "truncated $table"
                
        
                #get all data from source table
                $querySource = "Select * From $table"
                write-output $querySource

                $sqlcmdSource.CommandText = $querySource
                $dt = New-Object System.Data.DataTable
                $dt.Load($sqlcmdSource.ExecuteReader())

                write-output "loaded $table"

                
       
                #copy into destination table
                $bulkCopy = new-object ("Data.SqlClient.SqlBulkCopy") $connectionStringHistoric 
                $bulkCopy.DestinationTableName = $table 
                $bulkCopy.BatchSize = 50000 
                $bulkcopy.bulkcopyTimeout = 3600 
                $bulkCopy.WriteToServer($dt) 
                $dt.clear()

                write-output "bulk copied $table"
            }
            catch
            {
                $logText += "error replacing $table/r/n" 
            }
        }

        $sqlConnectionSource.Close()
        $sqlConnectionDest.Close() 

        ###############################################
   
        #setup siteIDS, DeptID expressions and table names
        $siteIDs = @(5,6,7,8,11,15,26,31,33,34,35,36,37,38,39,41,42,46)
     
        $deptIDs = @(' in (2,3,4)', ' in (2,3,4,5,7)', ' in (2,3,4,5,6)',' in (2,3)',' in (2,3,9)',' in (7)',' in (23,2)')
        for ($i=7; $i -lt $siteIDs.Length; $i++) 
        {
            $id = $siteIDs[$i]
            $deptIDs += " in (Select Distinct DepartmentID from Room Where SiteID = $id)"
        }
     
        $tableNames = @('NWH', 'WHC','Baystate', 'SWG', 'Liverpool', 'MGHGI', 'WMC', 'BIDMC', 'BSC', 
        'BIDMCCardiology', 'Medtronic', 'Gore', 'StJude', 'Cook', 'Virtua', 'Terumo', 'LLUH', 'VirtuaMarlton')

        # run query here for each site and drop it into appropriate destination table
        for ($i = 0; $i -lt $siteIDs.Length; $i++)
        {
            try
            {
                $siteID = $siteIDs[$i]
                $deptID = $deptIDs[$i]
                $tableName = $tableNames[$i]
    
                $sqlConnectionSource.Open()

                $query = "SELECT SiteID, DepartmentID, LotID, Quantity,
                '$backupDateAsDate' AS InventoryDate
                FROM HybridInventoryLotView v
                where Quantity <> 0 
                and Siteid = $siteID and Departmentid $deptID"

                Write-Output $query
            
                $sqlcmdSource.CommandText = $query
                $dt = New-Object System.Data.DataTable
                $dt.Load($sqlcmdSource.ExecuteReader())
                #$dt.data
                #$dt.log

                # Close connection to local db
                $sqlConnectionSource.Close()

                # need to add a column since we now have an identity column at position 0
                $dt.Columns.Add("ID","int").SetOrdinal(0)
    
                $sqlConnectionDest.Open() 

                $bulkCopy = new-object ("Data.SqlClient.SqlBulkCopy") $connectionStringDest 
                $bulkCopy.DestinationTableName = $tableName 
                $bulkCopy.BatchSize = 50000 
                $bulkcopy.bulkcopyTimeout = 300 
                $bulkCopy.WriteToServer($dt) 
                $dt.clear()

                $sqlConnectionDest.Close() 
            }
            catch
            {
                $logText += "error adding data for table $tableName/site $siteID/ dept $deptID/r/n"
            }
        }

        $sqlConnectionMaster = New-Object System.Data.SqlClient.SqlConnection
        $sqlConnectionMaster.ConnectionString = $connectionStringRestoredMaster
        $sqlcmdMaster = New-Object System.Data.SqlClient.SqlCommand
        $sqlcmdMaster.Connection = $sqlConnectionMaster

        $sqlConnectionMaster.open()
        $sqlcmdMaster.commandtext = "drop database [VueMedRestored]"
        $sqlcmdMaster.executenonquery()
        $sqlConnectionMaster.Close()

    }
    Catch
    {
        Write-Output $error
        $Subject = "failure in truncate and restore"
        $Body = "<p>Broke trying to truncate and restore</p><p>$error</p>"
        Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
    }
