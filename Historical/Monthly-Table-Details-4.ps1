 
 
 $connectionName = "AzureRunAsConnection"
       
    try 
    {
        # Get the connection "AzureRunAsConnection "
        $servicePrincipalConnection = Get-AutomationConnection -Name $connectionName
 
        "Login to Azure"
 
        Add-AzureRmAccount `
        -ServicePrincipal `
        -TenantId $servicePrincipalConnection.TenantId `
        -ApplicationId $servicePrincipalConnection.ApplicationId `
        -CertificateThumbprint $servicePrincipalConnection.CertificateThumbprint
    }
 
    catch 
    {
        if (!$servicePrincipalConnection) 
        {
            $ErrorMessage = "Connection $connectionName not found."
            throw $ErrorMessage
        }
        else 
        {
            Write-Error -Message $_.Exception
            throw $_.Exception
        }
    }

   
    $connectionString = (Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "vuemedhistoricalinventory-connection-string").SecretValueText
    $destdb = 'vuemedhistoricalinventory'

    $webhook = (Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "monthly-table-details-5-webhook").SecretValueText

    $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
    $command = new-object system.data.sqlclient.sqlcommand("",$connection)
        
    $DeptIds =   ( 7, 2, 3, 4, 3,  7, 28, 36, 39,  2, 23,  2)
    $Sites =     (15, 5, 5, 5, 6, 31, 34, 42, 42, 46, 26, 26)

    $date = Get-Date
    $year = $date.Year
    $month = $date.Month
        
    $thisMonth = Get-Date -Year $year -Month $month -Day 1 
    $month1 = ($thisMonth).AddMonths(-12)

    $thisMo = $thisMonth.ToString("yyyy-MM-dd")
    $mo1 = $month1.ToString("yyyy-MM-dd")

    $m0 = "'$thisMo'"
    $m1 = "'$mo1'"

    try
    {
        $connection.Open()

        for ($i = 0; $i -lt $DeptIds.Length; $i++)
        {
            $deptID = $DeptIDs[$i]
            $site = $Sites[$i]
            
            $query = "Exec KPIAddUnused @DepartmentID = $deptID, @SiteID = $site, @FromMonth = $m1, @ToMonth = $m0 "
            $command.CommandTimeout = 3600
            $command.CommandText = $query
            $command.ExecuteNonQuery()
        }

        #call the next runbook - Monthly-Table-Details-5
        Invoke-RestMethod -Method Post -Uri $webhook
    }
    catch
    {
        write-output "Unused1Year failed !!!"
    }
    finally
    {
        $connection.Close() 
    }

