    
    #this should run at 2:00am
    #wait until it finishes to run the other scripts
    
    $settingsXml = Get-AutomationVariable -Name SettingsHistoric
    [xml]$ConfigFile = $settingsXml

    $Conn = Get-AutomationConnection -Name AzureRunAsConnection
    Connect-AzureRmAccount -ServicePrincipal -Tenant $Conn.TenantID -ApplicationId $Conn.ApplicationID `
    -CertificateThumbprint $Conn.CertificateThumbprint
    
    $BaseStorageUri = $ConfigFile.Settings.Storage.baseStorageUri
    $DatabaseName = $ConfigFile.Settings.Database.restoreDatabaseName
    $UserName = $ConfigFile.Settings.Database.vuemed_production_sql_server_username
    $Password = $ConfigFile.Settings.Database.vuemed_production_sql_server_password
    $EmailFrom = $ConfigFile.Settings.Email.emailFromAutomatedBackup
    $EmailTo = $ConfigFile.Settings.Email.EmailToOps
    $SendGridPassword = ConvertTo-SecureString -String $ConfigFile.Settings.Email.SendGridUserPassword -AsPlainText -Force
    $SendGridUsername = $ConfigFile.Settings.Email.SendGridUserName
    $ServerName = $ConfigFile.Settings.Database.historicServer
    $SMTPServer = $ConfigFile.Settings.Email.SMTPServer
    $StorageKey = $ConfigFile.Settings.Storage.databasebackuphot_storage_access_key
    $ResourceGroupName = $ConfigFile.Settings.Database.ResourceGroupName

    $credential = New-Object System.Management.Automation.PSCredential $SendGridUsername, $SendGridPassword
    $securePassword = ConvertTo-SecureString -String $Password -AsPlainText -Force
    $context = New-AzureStorageContext -StorageAccountName 'databasebackuphot' -StorageAccountKey $StorageKey


    Try
    {
        $StorageKeytype = "StorageAccessKey"

        ############# change this when we start actually backing up to bacpacs????? ###############

        $blob = Get-AzureStorageBlob -Container 'backups-azure' -Context $context  -blob 'VueMed-*' | Sort CreationTime | Select -Last 1
        $blobName = $blob.Name
        
        $bacPacUri = $BaseStorageUri + $blobName

        $importRequest = New-AzureRmSqlDatabaseImport `
        -ResourceGroupName $ResourceGroupName `
        -ServerName $ServerName `
        -DatabaseName $DatabaseName `
        -StorageKeytype $StorageKeytype `
        -StorageKey $StorageKey `
        -StorageUri $bacPacUri `
        -AdministratorLogin $UserName `
        -AdministratorLoginPassword $securePassword `
        -Edition Standard `
        -ServiceObjectiveName 'S6' `
        -DatabaseMaxSizeBytes 268435456000

        $date = ((Get-Date).AddHours(-7)).ToString("yyyy-MM-dd-HH-mm")
        $Subject = "Nightly restore request initiated"
        $Body = "<p>Vuemed bacpac restore to testsever operation has been initiated</p><p>bacpac = $blobName</p><p>$date Pacific Daylight Time (UTC - 7)</p>"
        Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml

    }
    Catch
    {
        Write-Output $error
        $Subject = "failure restoring Vuemed"
        $Body = "<p>Broke trying to restore VueMed to testserver</p><p>$error</p>"
        Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
    }
