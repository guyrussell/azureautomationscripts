    
    $Conn = Get-AutomationConnection -Name AzureRunAsConnection
    Connect-AzureRmAccount -ServicePrincipal -Tenant $Conn.TenantID -ApplicationId $Conn.ApplicationID `
    -CertificateThumbprint $Conn.CertificateThumbprint
    
    $settingsXml = Get-AutomationVariable -Name SettingsHistoric
    [xml]$ConfigFile = $settingsXml

    $connectionStringHistoric = $ConfigFile.Settings.Database.connectionStringHistoric
    $connectionHistoric = new-object system.data.SqlClient.SQLConnection($connectionStringHistoric)
    $commandHistoric = new-object system.data.sqlclient.sqlcommand("", $connectionHistoric)


    Function AppendHistoricData()
    {
        param
        (
            [string]$Sproc = ''
        )

        Try
        {
            $connectionHistoric.Open()
            $commandHistoric.CommandText = $Sproc
            $commandHistoric.CommandTimeout = 3600
            $commandHistoric.ExecuteNonQuery()
        }
        Catch
        {
            Write-Output "Failure executing $Sproc"

            Write-Output $error
        }
        Finally
        {
            $connectionHistoric.Close()
        }
}


   
    #################################### bid #############################################################################

    $storedProcedure = "exec AppendBIDMCCardiology"
    AppendHistoricData -Sproc $storedProcedure 
    


# ###################################### lluh ##########################################################################
   
    $storedProcedure = "exec AppendLLUH"
    AppendHistoricData -Sproc $storedProcedure 
    
