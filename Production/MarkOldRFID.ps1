
    $Conn = Get-AutomationConnection -Name AzureRunAsConnection
    Connect-AzureRmAccount -ServicePrincipal -Tenant $Conn.TenantID -ApplicationId $Conn.ApplicationID `
    -CertificateThumbprint $Conn.CertificateThumbprint

    $settingsXml = Get-AutomationVariable -Name SettingsXML
    [xml]$ConfigFile = $settingsXml

    $SendGridUsername = $ConfigFile.Settings.Email.SendGridUserName
    $SendGridPassword = ConvertTo-SecureString -String $ConfigFile.Settings.Email.SendGridUserPassword -AsPlainText -Force
    $credential = New-Object System.Management.Automation.PSCredential $SendGridUsername, $SendGridPassword
    $SMTPServer = $ConfigFile.Settings.Email.SMTPServer
    $EmailFrom = $ConfigFile.Settings.Email.EmailFromSupport
    $EmailTo = $ConfigFile.Settings.Email.EmailToSupport
    
    $connectionString = $ConfigFile.Settings.Database.vuemed_production_sql_server_vuemed_connection_string
    $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
    $command = new-object system.data.sqlclient.sqlcommand("",$connection)
    $connection.Open()

    $command.CommandText = 'EXEC MarkOldRFID'

    $command.CommandTimeout = 3600
    
    Try
    {
        $command.ExecuteNonQuery()

        Write-Output 'Success!'
    }
    Catch
    {
        Write-Output 'Failure!'
        Write-Output $error
    }
    Finally
    {
        $connection.Close();
    }


