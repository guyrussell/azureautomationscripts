
    $Conn = Get-AutomationConnection -Name AzureRunAsConnection
    Connect-AzureRmAccount -ServicePrincipal -Tenant $Conn.TenantID -ApplicationId $Conn.ApplicationID `
    -CertificateThumbprint $Conn.CertificateThumbprint

    $settingsXml = Get-AutomationVariable -Name SettingsXML
    [xml]$ConfigFile = $settingsXml

    $connectionString = $ConfigFile.Settings.Database.vuemed_production_sql_server_vuemed_connection_string
    $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
    $command = new-object system.data.sqlclient.sqlcommand("",$connection)
    $command.CommandTimeout = 3600
    $connection.Open()
    
    Try
    {
        $command.CommandText = "Exec Reset_DoNotReorder_Flags_WMC"

        $command.ExecuteNonQuery()

        Write-Output 'Success!'
    }
    Catch
    {
        Write-Output 'Failure!'
        Write-Output $error
    }
    Finally
    {
        $connection.Close();
    }

