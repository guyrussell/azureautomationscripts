
    $Conn = Get-AutomationConnection -Name AzureRunAsConnection
    Connect-AzureRmAccount -ServicePrincipal -Tenant $Conn.TenantID -ApplicationId $Conn.ApplicationID `
    -CertificateThumbprint $Conn.CertificateThumbprint

    $settingsXml = Get-AutomationVariable -Name SettingsXML
    [xml]$ConfigFile = $settingsXml

    $StorageKey = $ConfigFile.Settings.Storage.vuemed_blob_primary_access_key
    $context = New-AzureStorageContext -StorageAccountName "vuemedblob" -StorageAccountKey $StorageKey

    $files = ('ProductCatalogMerge-2*', 'ProductCatalogMerge-7*', 'ProductCatalogMerge-8*', 'ProductCatalogMerge-9*', 
            'ProductCatalogMerge-10*', 'ProductCatalogMerge-11*', 'ProductCatalogMerge-12*', 'ProductCatalogExport*')
    
    for ($i = 0; $i -lt $files.Count; $i++)
    {
        $blobs = Get-AzureStorageBlob -Container "product-catalog" -Context $context -Blob $($files[$i])
    
        $count = $blobs.Count

        if ($count -gt 2)
        {
            $DatetoDelete = (Get-Date).AddDays(-3)

            foreach ($blob in $blobs)
            {
                If ($blob.LastModified -lt $DateToDelete)
                {
                    $str = "Deleteing " + $blob.Name
                    Write-Output $str

                    Remove-AzureStorageBlob -Blob $blob.Name -Container "product-catalog" -Context $context
                }
                else
                {
                    $str = "Keeping " + $blob.Name
                    Write-Output $str
                }
            }
        }
    }





