
    $Conn = Get-AutomationConnection -Name AzureRunAsConnection
    Connect-AzureRmAccount -ServicePrincipal -Tenant $Conn.TenantID -ApplicationId $Conn.ApplicationID `
    -CertificateThumbprint $Conn.CertificateThumbprint

    $settingsXml = Get-AutomationVariable -Name SettingsXML
    [xml]$ConfigFile = $settingsXml

    $day = (Get-Date).DayOfWeek.value__

    if ($day -lt 6)
    {

        $SendGridUsername = $ConfigFile.Settings.Email.SendGridUserName
        $SendGridPassword = ConvertTo-SecureString -String $ConfigFile.Settings.Email.SendGridUserPassword -AsPlainText -Force
        $credential = New-Object System.Management.Automation.PSCredential $SendGridUsername, $SendGridPassword
        $SMTPServer = $ConfigFile.Settings.Email.SMTPServer
        $EmailTo = $ConfigFile.Settings.Email.emailToSupport
        $EmailFrom = $ConfigFile.Settings.Email.emailFromSupport
        $connectionString = $ConfigFile.Settings.Database.vuemed_production_sql_server_vuemed_connection_string
        
        $Subject = "Liverpool sync status"

        
        $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
        $command = new-object system.data.sqlclient.sqlcommand("",$connection)
        $connection.Open()

        $command.CommandText = "Exec LiverpoolSyncStatusHTML"
        
        $command.CommandTimeout = 240

        $val = $command.ExecuteScalar()

        $connection.Close()

        if ($val.length -gt 0)
        {
            $Body = $val
            Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
        }
    }

