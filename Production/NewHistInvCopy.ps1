

    

    $connectionName = "AzureRunAsConnection"
       
    try 
    {
        # Get the connection "AzureRunAsConnection "
        $servicePrincipalConnection = Get-AutomationConnection -Name $connectionName
 
        "Login to Azure"
 
        Add-AzureRmAccount `
        -ServicePrincipal `
        -TenantId $servicePrincipalConnection.TenantId `
        -ApplicationId $servicePrincipalConnection.ApplicationId `
        -CertificateThumbprint $servicePrincipalConnection.CertificateThumbprint
    }
 
    catch 
    {
        if (!$servicePrincipalConnection) 
        {
            $ErrorMessage = "Connection $connectionName not found."
            throw $ErrorMessage
        }
        else 
        {
            Write-Error -Message $_.Exception
            throw $_.Exception
        }
    }

    $SendGridUsername =(Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "Send-Grid-User-Name").SecretValueText
    $SendGridPassword = ConvertTo-SecureString -String (Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "Send-Grid-Password").SecretValueText -AsPlainText -Force
    $credential = New-Object System.Management.Automation.PSCredential $SendGridUsername, $SendGridPassword
    $SMTPServer = "smtp.sendgrid.net"
    $EmailFrom = "automated-data-harvest@vuemed.com"
    $EmailTo = "canderson@vuemed.com, grussell@vuemed.com"

    $backupdate =  (Get-Date).ToString("yyyy-MM-dd")

    # later after transfer to azure proper, connect here
    #$connectionStringSource = (Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "vuemed-production-sql-server-vuemed-connection-string").SecretValueText
    
    # for now using a connection to sql server on prod
    $connectionStringSource = (Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "VMSqlRemoteConnectionString").SecretValueText
    

    # later after transfer to azure proper, connect here
    $connectionStringDestination = (Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "vuemedhistoricalinventory-connection-string").SecretValueText
    
    # for now using a connection to a test database
    #$connectionStringDestination = (Get-AzureKeyVaultSecret –VaultName "VueMedKeyVaultHSM" –Name "vuemedhistoricalinventorytest-connectionstring").SecretValueText
    

    # later after transfer to azure proper, use this db
    #$destdb = 'vuemedhistoricalinventoryb'

    #for now, using test
    $destdb = 'vuemedhistoricalinventory'



    $connectionSource = new-object system.data.SqlClient.SQLConnection($connectionStringSource)
    $commandSource = new-object system.data.sqlclient.sqlcommand("",$connectionSource)
    $connectionDestination = new-object system.data.SqlClient.SQLConnection($connectionStringDestination)
    $commandDestination = new-object system.data.sqlclient.sqlcommand("",$connectionDestination)
    

    #Import-Module dbatools?????
    

    #setup siteIDS, DeptID expressions and table names
    #cannot reorder without reordering deptids and table names!!
    $siteIDs = @(5,6,7,8,11,15,26,31,33,34,35,36,37,38,39,41,42,46)
     
    #cannot reorder without reordering siteids and table names!!
    $deptIDs = @(' in (2,3,4)',' in (2,3,4,5,7)',' in (2,3,4,5,6)',' in (2,3)',' in (2,3,9)',' in (7)',' in (23,2)') 

    for ($i=7; $i -lt $siteIDs.Length; $i++) 
    {
        $id = $siteIDs[$i]
        $deptIDs += " in (Select Distinct DepartmentID from Room Where SiteID = $id)"
    }

    #cannot reorder without reordering siteids and deptids!!
    $tableNames = @('NWH','WHC','Baystate','SWG','Liverpool','MGHGI','WMC','BIDMC','BSC','BIDMCCardiology', 
    'Medtronic','Gore','StJude','Cook','Virtua','Terumo','LLUH','VirtuaMarlton') 
        

    # run query here for each site and drop it into appropriate destination table
    for ($i = 0; $i -lt $siteIDs.Length; $i++)
    {
        $siteID = $siteIDs[$i]
        $deptID = $deptIDs[$i]
        $tableName = $tableNames[$i]
    
        $connectionSource.Open()

        $query = "SELECT lv.SiteID, s.[name] as SiteName, lv.DepartmentID, d.[Name] as DepartmentName, 'N/A' as RoomName, `
        l.ProductID, l.LotID, l.LotNumber, l.ExpirationDate, lv.quantity, pv.ManufacturerName, pv.CategoryName,  `
        pv.ProductType, pv.ProductName, pv.ManufacturerProductCode, pv.Diameter, pv.[Length], pv.PrimaryFeature,  `
        pv.SecondaryFeature, pv.OtherFeature, dpd.DepartmentProductDataID, dpd.ParLevel, dpd.ReorderLevel,  `
        dpd.price, dpd.Consigned, dpd.IgnoreLot, dpd.IgnoreInventory, '$backupdate' AS InventoryDate `
        FROM HybridInventoryLotView lv `
        inner join lot l on lv.lotid = l.lotid `
        inner join site s on s.siteid = lv.siteid `
        inner join department d on d.DepartmentID = lv.DepartmentID `
        inner join ProductView pv on pv.ProductID = l.ProductID `
        left join DepartmentProductData dpd on dpd.SiteID = lv.siteid  `
        and dpd.departmentid = lv.departmentid  `
        and dpd.productid = pv.productid `
        where lv.quantity <> 0  `
        and lv.siteid = $siteID and lv.departmentid $deptID `
        "

        $commandSource.CommandText = $query
        $dt = New-Object System.Data.DataTable
        $dt.Load($commandSource.ExecuteReader())

        # Close connection to source db
        $connectionSource.Close()

        # need to add a column since we now have an identity column at position 0
        $dt.Columns.Add("id","int").SetOrdinal(0)
        
        $connectionDestination.Open()

        $bulkCopy = new-object ("Data.SqlClient.SqlBulkCopy") $connectionStringDestination 
        $bulkCopy.DestinationTableName = $tableName 
        $bulkCopy.BatchSize = 50000 
        $bulkcopy.bulkcopyTimeout = 300  
        $bulkCopy.WriteToServer($dt) 
        
        $dt.clear()

        $connectionDestination.Close() 
    }


    $tableNamesTruncate = @('Product', 'Lot', 'Manufacturer', 'ProductType', 'Category', 'Site', 'Department', 'HistoricalPrice') 

    # run query here for each table and drop it into appropriate destination table
    for ($i = 0; $i -lt $tableNamesTruncate.Length; $i++)
    {
        $tableName = $tableNamesTruncate[$i]
    
        $connectionSource.Open()

        $querySource = "SELECT * From $tableName"

        $commandSource.CommandText = $querySource
        $dt = New-Object System.Data.DataTable
        $dt.Load($commandSource.ExecuteReader())

        # Close connection to source db
        $connectionSource.Close()

        $connectionDestination.Open()
        Try
        {
            $queryDestination = "Truncate Table $tableName" 
            $commandDestination.CommandText = $queryDestination
            $commandDestination.ExecuteNonQuery()       

            $bulkCopy = new-object ("Data.SqlClient.SqlBulkCopy") $connectionStringDestination 
            $bulkCopy.DestinationTableName = $tableName 
            $bulkCopy.BatchSize = 50000 
            $bulkcopy.bulkcopyTimeout = 300  
            $bulkCopy.WriteToServer($dt) 
            
            $dt.clear()
        }
        Catch{}

        $connectionDestination.Close() 
    }















