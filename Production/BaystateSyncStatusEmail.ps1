    $settingsXml = Get-AutomationVariable -Name SettingsXML
    [xml]$ConfigFile = $settingsXml

    $Conn = Get-AutomationConnection -Name AzureRunAsConnection
    Connect-AzureRmAccount -ServicePrincipal -Tenant $Conn.TenantID -ApplicationId $Conn.ApplicationID `
    -CertificateThumbprint $Conn.CertificateThumbprint

    $connectionString = $ConfigFile.Settings.Database.vuemed_production_sql_server_vuemed_connection_string
    $EmailFrom = $ConfigFile.Settings.Email.emailFromOps
    $EmailTo = $ConfigFile.Settings.Email.emailToSupport
    $SendGridUsername = $ConfigFile.Settings.Email.SendGridUserName
    $SendGridPassword = ConvertTo-SecureString -String $ConfigFile.Settings.Email.SendGridUserPassword -AsPlainText -Force
    #change this to SendGridServer?
    $SMTPServer = $ConfigFile.Settings.Email.SMTPServer

    $day = (Get-Date).DayOfWeek.value__

    if ($day -lt 6)
    {
        $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
        $command = new-object system.data.sqlclient.sqlcommand("",$connection)
        $command.CommandTimeout = 120

        $command.CommandText = 'EXEC BaystateSyncStatusHTML '
        $val = ''
   
        $connection.Open()

        try
        {
            $val = $command.ExecuteScalar()
        }
        Finally
        {
            $connection.Close()
        }

        if ($val.length -gt 0)
        {
            $credential = New-Object System.Management.Automation.PSCredential $SendGridUsername, $SendGridPassword
            $Subject = "Baystate sync status"

            $Body = $val
            Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
        }
    }
    













