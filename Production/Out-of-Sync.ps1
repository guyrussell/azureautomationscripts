

    $Conn = Get-AutomationConnection -Name AzureRunAsConnection
    Connect-AzureRmAccount -ServicePrincipal -Tenant $Conn.TenantID -ApplicationId $Conn.ApplicationID `
    -CertificateThumbprint $Conn.CertificateThumbprint

    $settingsXml = Get-AutomationVariable -Name SettingsXML
    [xml]$ConfigFile = $settingsXml

    $SendGridUsername = $ConfigFile.Settings.Email.SendGridUserName
    $SendGridPassword = ConvertTo-SecureString -String $ConfigFile.Settings.Email.SendGridUserPassword -AsPlainText -Force
    $credential = New-Object System.Management.Automation.PSCredential $SendGridUsername, $SendGridPassword
    $SMTPServer = $ConfigFile.Settings.Email.SMTPServer
    $EmailFrom = $ConfigFile.Settings.Email.EmailFromSupport
    $EmailTo = $ConfigFile.Settings.Email.EmailToSupport
    $connectionString = $ConfigFile.Settings.Database.vuemed_production_sql_server_vuemed_connection_string
    
    $Subject = "out of sync"

    $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
    $command = new-object system.data.sqlclient.sqlcommand("",$connection)
    $command.CommandTimeout = 300

    #took out removebatch
    $sets = ("Remove", "TransferBatch", "Transfer", "IncomingBatch", 
                "Incoming", "Procedure", "Outgoing")
    
    $query = "Exec OutOfSync "
    $command.CommandText = $query
    $connection.Open()

    # query returns 8 datatables
    $ds = New-Object System.Data.Dataset
    $da = New-Object System.Data.SqlClient.SqlDataAdapter 
    $da.SelectCommand = $command
    $da.Fill($ds)
    
    # Close connection to source db
    $connection.Close()

    $hasData = $false
    $html = ""

    for ($i = 0; $i -lt $ds.Tables.Count; $i++)
    {
        $dt = $ds.Tables[$i]

        if ($dt.Rows.Count -gt 0)
        {
            $hasData = $true

            $set = $sets[$i]

            $html += "</br></br><b>$set</b></br>"

            $html += "<table cellpadding='2' cellspacing='2'><tr><td>MachineID</td><td>RoomID</td><td>Description</td><td>RoomName</td><td>Count</td></tr>"

            foreach ($row in $dt.Rows)
            { 
                $html += "<tr><td>" + $row[0] + "</td><td>" + $row[1] + "</td><td>" + $row[2] + "</td><td>" + $row[3] + "</td><td>" + $row[4] + "</td></tr>"
            }

            $html += "</table>"
        }
    }

    if ($hasData -eq $true)
    {
        $Body = $html
        Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
    }
    





    












