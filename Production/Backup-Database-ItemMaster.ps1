
    $settingsXml = Get-AutomationVariable -Name SettingsXML
    [xml]$ConfigFile = $settingsXml

    $Conn = Get-AutomationConnection -Name AzureRunAsConnection
    Connect-AzureRmAccount -ServicePrincipal -Tenant $Conn.TenantID -ApplicationId $Conn.ApplicationID `
    -CertificateThumbprint $Conn.CertificateThumbprint

    $SendGridUsername = $ConfigFile.Settings.Email.SendGridUserName
    $SendGridPassword = ConvertTo-SecureString -String $ConfigFile.Settings.Email.SendGridUserPassword -AsPlainText -Force
    $EmailTo = $ConfigFile.Settings.Email.EmailToOps
    $ServerAdmin = $ConfigFile.Settings.Database.vuemed_production_sql_server_username
    $securePassword = ConvertTo-SecureString -String $ConfigFile.Settings.Database.vuemed_production_sql_server_password -AsPlainText -Force
    $StorageKey = $ConfigFile.Settings.Storage.databasebackuphot_storage_access_key
    $DatabaseName = $ConfigFile.Settings.Database.itemMasterDatabaseName
    $ServerName = $ConfigFile.Settings.Database.productionServer
    $ResourceGroupName = $ConfigFile.Settings.Database.productionResourceGroup
    $BaseStorageUri = $ConfigFile.Settings.Storage.baseStorageUri
    $SMTPServer = $ConfigFile.Settings.Email.SMTPServer
    $EmailFrom = $ConfigFile.Settings.Email.emailFromAutomatedBackup

    Try
    {
        # Generate a unique filename for the BACPAC
        $bacpacFilename = $DatabaseName + "-" + ((Get-Date).AddHours(-7)).ToString("yyyy-MM-dd-HH-mm") + ".bacpac"

        $BacpacUri = $BaseStorageUri + $bacpacFilename
        
        $StorageKeytype = "StorageAccessKey"
    
        $exportRequest = New-AzureRmSqlDatabaseExport -ResourceGroupName $ResourceGroupName -ServerName $ServerName `
        -DatabaseName $DatabaseName -StorageKeytype $StorageKeytype -StorageKey $StorageKey -StorageUri $BacpacUri `
        -AdministratorLogin $ServerAdmin -AdministratorLoginPassword $securePassword

        Write-Output "successfully submitted request"
    }
    Catch
    {
        $credential = New-Object System.Management.Automation.PSCredential $SendGridUsername, $SendGridPassword
        $date = ((Get-Date).AddHours(-7)).ToString("yyyy-MM-dd-HH-mm")
        $Subject = "itemmaster database export failed"
        $Body = "<p>failure exporting itemmaster database as bacpac to storage</p><p>$date Pacific Daylight Time</p><p>$error</p>"
        Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
    }

       

