

    $Conn = Get-AutomationConnection -Name AzureRunAsConnection
    Connect-AzureRmAccount -ServicePrincipal -Tenant $Conn.TenantID -ApplicationId $Conn.ApplicationID `
    -CertificateThumbprint $Conn.CertificateThumbprint

    $settingsXml = Get-AutomationVariable -Name SettingsXML
    [xml]$ConfigFile = $settingsXml


    $connectionString = $ConfigFile.Settings.Database.vuemed_production_sql_server_itemmaster_connection_string

    $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
    $command = new-object system.data.sqlclient.sqlcommand("",$connection)#weird initializer
    $connection.Open()

    $command.CommandText = 'EXEC ItemMasterUpdate' 

    # every day between 6am and 10 pm
    # this is called from an Azure Function App
    # AutomatedTimers - ItemMasterUpdateAzureWebHook and will need to be enabled

    $command.CommandTimeout = 3600
    
    Try
    {
        $command.ExecuteNonQuery()

        Write-Output 'Success!'
    }
    Catch
    {
        Write-Output 'Failure!'
        Write-Output $error
    }
    Finally
    {
        $connection.Close();
    }

