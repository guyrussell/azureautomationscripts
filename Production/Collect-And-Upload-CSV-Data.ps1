
    $Conn = Get-AutomationConnection -Name AzureRunAsConnection
    Connect-AzureRmAccount -ServicePrincipal -Tenant $Conn.TenantID -ApplicationId $Conn.ApplicationID `
    -CertificateThumbprint $Conn.CertificateThumbprint
    
    $settingsXml = Get-AutomationVariable -Name SettingsXML
    [xml]$ConfigFile = $settingsXml

    $storageKeyBID = $ConfigFile.Settings.Storage.bidmcadmin_storage_primary_access_key
    $storageKeyLLUH = $ConfigFile.Settings.Storage.lluhadmin_storage_primary_access_key
    $storageKeyJJ = $ConfigFile.Settings.Storage.jjadmin_storage_primary_access_key
    $storageKeyWMC = $ConfigFile.Settings.Storage.wmcadmin_storage_primary_access_key
    $storageKeyMGH = $ConfigFile.Settings.Storage.mghadmin_storage_primary_access_key

    $connectionString = $ConfigFile.Settings.Database.vuemed_production_sql_server_vuemed_connection_string
    $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
    $command = new-object system.data.sqlclient.sqlcommand("", $connection)

    Function CollectCSVDataAndUploadToAzureStorage()
    {
        param
        (
            [string]$Sproc = '',
            [string]$StorageAccountName = '',
            [string]$StorageAccountKey = '',
            [string]$FileName = '',
            [string]$StorageContainer  = ''           
        )

        Try
        {
            $context = New-AzureStorageContext -StorageAccountName $StorageAccountName -StorageAccountKey $StorageAccountKey

            $connection.Open()

            $command.CommandText = $Sproc
            $command.CommandTimeout = 3600
            $SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter
            $SqlAdapter.SelectCommand = $command
            $DataTable = New-Object System.Data.DataTable
            $SqlAdapter.Fill($DataTable)
            $DataTable | Export-Csv -NoTypeInformation $FileName -Delimiter "|"
            Set-AzureStorageBlobContent -File $FileName -Container $storageContainer -Blob $FileName -Context $context -Force
        }
        Catch
        {
            Write-Output "Failure creating and uploading $fileName"

            Write-Output $error
        }
        Finally
        {
            $connection.Close()
        }
}
   
    #################################### bid #############################################################################

    $container = 'bidcsvfiles2'
    $accountName = 'bidmcadmin'

    $storedProcedure = 'EXEC GetHybridInventoryBySite @site1 = 34, @site2 = 31'
    $extractFile = 'BIDMCInventoryView.csv'
    CollectCSVDataAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyBID -FileName $extractFile -StorageContainer $container
    
    $storedProcedure = "EXEC GetOutgoingForAzureStorage @site1 = 34, @site2 = 31"
    $extractFile = 'BIDMCOutgoingView.csv'
    CollectCSVDataAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyBID -FileName $extractFile -StorageContainer $container
    
    $storedProcedure = "EXEC GetRemoveForAzureStorage @site1 = 34, @site2 = 31"
    $extractFile = 'BIDMCRemoveView.csv'
    CollectCSVDataAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyBID -FileName $extractFile -StorageContainer $container
    
    $storedProcedure = "EXEC GetIncomingForAzureStorage_new @site1 = 34, @site2 = 31"
    $extractFile = 'BIDMCIncomingView.csv'
    CollectCSVDataAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyBID -FileName $extractFile -StorageContainer $container
    
# ###################################### lluh ##########################################################################
   
    $container = 'lluhcsvfiles2'
    $accountName = 'lluhadmin'

    # $storedProcedure = "exec GetHybridInventoryBySite @site1 = 42"
    # $extractFile = 'LLUHInventoryView.csv'
    # CollectCSVDataAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyLLUH -FileName $extractFile -StorageContainer $container
    
    $storedProcedure = "exec GetOutgoingForAzureStorage @site1 = 42"
    $extractFile = 'LLUHOutgoingView.csv'
    CollectCSVDataAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyLLUH -FileName $extractFile -StorageContainer $container
    
    $storedProcedure = "exec GetRemoveForAzureStorage @site1 = 42"
    $extractFile = 'LLUHRemoveView.csv'
    CollectCSVDataAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyLLUH -FileName $extractFile -StorageContainer $container
    
    $storedProcedure = "exec GetIncomingForAzureStorage_new @site1 = 42"
    $extractFile = 'LLUHIncomingView.csv'
    CollectCSVDataAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyLLUH -FileName $extractFile -StorageContainer $container
    
    $storedProcedure = "exec GetTransferForAzureStorage @site1 = 42"
    $extractFile = 'LLUHTransferView.csv'
    CollectCSVDataAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyLLUH -FileName $extractFile -StorageContainer $container
    
        
####################################### jj ##########################################################################

    $container = 'jjcsvfiles'
    $accountName = 'jjadmin'

    $storedProcedure = "exec GetHybridInventoryByManufacturer @site1 = 34, @site2 = 31, @manufacturer = 92"
    $extractFile = 'JJInventoryView.csv'
    CollectCSVDataAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyJJ -FileName $extractFile -StorageContainer $container
    
    $storedProcedure = "exec GetOutgoingForAzureStorageByManufacturer @site1 = 34, @site2 = 31, @manufacturer = 92"
    $extractFile = 'JJOutgoingView.csv'
    CollectCSVDataAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyJJ -FileName $extractFile -StorageContainer $container
    
    $storedProcedure = "exec GetRemoveForAzureStorageByManuacturer @site1 = 34, @site2 = 31, @manufacturer = 92"
    $extractFile = 'JJRemoveView.csv'
    CollectCSVDataAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyJJ -FileName $extractFile -StorageContainer $container
    
    $storedProcedure = "exec GetIncomingForAzureStorageByManufacturer @site1 = 34, @site2 = 31, @manufacturer = 92"
    $extractFile = 'JJIncomingView.csv'
    CollectCSVDataAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyJJ -FileName $extractFile -StorageContainer $container
    
    $storedProcedure = "exec GetTransferForAzureStorageByManufacturer @site1 = 34, @site2 = 31, @manufacturer = 92"
    $extractFile = 'JJTransferView.csv'
    CollectCSVDataAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyJJ -FileName $extractFile -StorageContainer $container
    
# ####################################### wmc ##########################################################################

    $container = 'wmccsvfiles'
    $accountName = 'wmcadmin'

    $storedProcedure = "exec GetHybridInventoryBySite @site1 = 26"
    $extractFile = 'WMCInventoryView.csv'
    CollectCSVDataAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyWMC -FileName $extractFile -StorageContainer $container
    
    $storedProcedure = "exec GetOutgoingForAzureStorage @site1 = 26"
    $extractFile = 'WMCOutgoingView.csv'
    CollectCSVDataAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyWMC -FileName $extractFile -StorageContainer $container
    
    $storedProcedure = "exec GetRemoveForAzureStorage @site1 = 26"
    $extractFile = 'WMCRemoveView.csv'
    CollectCSVDataAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyWMC -FileName $extractFile -StorageContainer $container
    
    $storedProcedure = "exec GetIncomingForAzureStorage_new @site1 = 26"
    $extractFile = 'WMCIncomingView.csv'
    CollectCSVDataAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyWMC -FileName $extractFile -StorageContainer $container
    
    $storedProcedure = "exec GetTransferForAzureStorage @site1 = 26"
    $extractFile = 'WMCTransferView.csv'
    CollectCSVDataAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyWMC -FileName $extractFile -StorageContainer $container
    
    $storedProcedure = "exec GetTransferWithIncomingForAzureStorage @site1 = 26"
    $extractFile = 'WMCCombinedIncomingTransferView.csv'
    CollectCSVDataAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyWMC -FileName $extractFile -StorageContainer $container
    
# ####################################### mgh ##########################################################################

    $container = 'mghcsvfiles'
    $accountName = 'mghadmin'

    $storedProcedure = "exec GetHybridInventoryBySite @site1 = 15"
    $extractFile = 'MGHInventoryView.csv'
    CollectCSVDataAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyMGH -FileName $extractFile -StorageContainer $container
    
    $storedProcedure = "exec GetOutgoingForAzureStorage @site1 = 15"
    $extractFile = 'MGHOutgoingView.csv'
    CollectCSVDataAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyMGH -FileName $extractFile -StorageContainer $container
    
    $storedProcedure = "exec GetRemoveForAzureStorage @site1 = 15"
    $extractFile = 'MGHRemoveView.csv'
    CollectCSVDataAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyMGH -FileName $extractFile -StorageContainer $container
    
    $storedProcedure = "exec GetIncomingForAzureStorage_new @site1 = 15"
    $extractFile = 'MGHIncomingView.csv'
    CollectCSVDataAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyMGH -FileName $extractFile -StorageContainer $container
    
    $storedProcedure = "exec GetTransferForAzureStorage @site1 = 15"
    $extractFile = 'MGHTransferView.csv'
    CollectCSVDataAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyMGH -FileName $extractFile -StorageContainer $container
    
    $storedProcedure = "exec GetTransferWithIncomingForAzureStorage @site1 = 15"
    $extractFile = 'MGHCombinedIncomingTransferView.csv'
    CollectCSVDataAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyMGH -FileName $extractFile -StorageContainer $container




