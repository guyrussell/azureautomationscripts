    
    $Conn = Get-AutomationConnection -Name AzureRunAsConnection
    Connect-AzureRmAccount -ServicePrincipal -Tenant $Conn.TenantID -ApplicationId $Conn.ApplicationID `
    -CertificateThumbprint $Conn.CertificateThumbprint
    
    $settingsXml = Get-AutomationVariable -Name SettingsXML
    [xml]$ConfigFile = $settingsXml

    $storageKeyBID = $ConfigFile.Settings.Storage.bidmcadmin_storage_primary_access_key
    $storageKeyLLUH = $ConfigFile.Settings.Storage.lluhadmin_storage_primary_access_key
    $storageKeyJJ = $ConfigFile.Settings.Storage.jjadmin_storage_primary_access_key
    $storageKeyWMC = $ConfigFile.Settings.Storage.wmcadmin_storage_primary_access_key
    $storageKeyMGH = $ConfigFile.Settings.Storage.mghadmin_storage_primary_access_key

    $connectionStringHistoric = $ConfigFile.Settings.Database.connectionStringHistoric
    $connectionHistoric = new-object system.data.SqlClient.SQLConnection($connectionStringHistoric)
    $commandHistoric = new-object system.data.sqlclient.sqlcommand("", $connectionHistoric)


    Function CollectCSVDataFromHistoricAndUploadToAzureStorage()
    {
        param
        (
            [string]$Sproc = '',
            [string]$StorageAccountName = '',
            [string]$StorageAccountKey = '',
            [string]$FileName = '',
            [string]$StorageContainer  = ''           
        )

        Try
        {
            $context = New-AzureStorageContext -StorageAccountName $StorageAccountName -StorageAccountKey $StorageAccountKey

            $connectionHistoric.Open()

            $commandHistoric.CommandText = $Sproc
            $commandHistoric.CommandTimeout = 3600

            $commandHistoric.ExecuteNonQuery()

            # $SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter
            # $SqlAdapter.SelectCommand = $commandHistoric
            # $DataTable = New-Object System.Data.DataTable
            # $SqlAdapter.Fill($DataTable)

            # export-csv –append –path \\Archive01\Scripts\Scripts.csv
            
            # $DataTable | Export-Csv -NoTypeInformation $FileName -Delimiter "|"
            # Set-AzureStorageBlobContent -File $FileName -Container $storageContainer -Blob $FileName -Context $context -Force
        }
        Catch
        {
            Write-Output "Failure creating and uploading $fileName"

            Write-Output $error
        }
        Finally
        {
            $connectionHistoric.Close()
        }
}


   
    #################################### bid #############################################################################

    $container = 'bidcsvfiles2'
    $accountName = 'bidmcadmin'

    $storedProcedure = "exec AppendBIDMCCardiology"
    $extractFile = 'HistoricalInventoryView.csv'
    CollectCSVDataFromHistoricAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyLLUH -FileName $extractFile -StorageContainer $container
    


# ###################################### lluh ##########################################################################
   
    $container = 'lluhcsvfiles2'
    $accountName = 'lluhadmin'

    $storedProcedure = "exec AppendLLUH"
    $extractFile = 'HistoricalInventoryView.csv'
    CollectCSVDataFromHistoricAndUploadToAzureStorage -Sproc $storedProcedure -StorageAccountName $accountName -StorageAccountKey $storageKeyLLUH -FileName $extractFile -StorageContainer $container
    
