
$settingsXml = Get-AutomationVariable -Name SettingsXML
    [xml]$ConfigFile = $settingsXml

    $Conn = Get-AutomationConnection -Name AzureRunAsConnection
    Connect-AzureRmAccount -ServicePrincipal -Tenant $Conn.TenantID -ApplicationId $Conn.ApplicationID `
    -CertificateThumbprint $Conn.CertificateThumbprint

    $connectionString = $ConfigFile.Settings.Database.vuemed_production_sql_server_vuemed_connection_string
    $StorageKeyBlob = $ConfigFile.Settings.Storage.vuemed_blob_primary_access_key

    Import-Module PSSQLite


    Function GZip-File
    {
        Param
        (
            [String]$fileToCompress,
            [String]$compressedFile
        )

        try
        {
            $fsIn = New-Object System.IO.FileStream($fileToCompress, [System.IO.FileMode]::Open, [System.IO.FileAccess]::Read, [System.IO.FileShare]::Read)
            $fsOut = New-Object System.IO.FileStream($compressedFile, [System.IO.FileMode]::CreateNew, [System.IO.FileAccess]::Write, [System.IO.FileShare]::None)
            $gzStream = New-Object System.IO.Compression.GZipStream($fsout, [System.IO.Compression.CompressionMode]::Compress)
        
            $buffer = New-Object byte[](262144) #256KB
            do
            {
                #read into buffer
                $read = $fsIn.Read($buffer,0,262144)
                #write buffer back out
                $gzStream.Write($buffer,0,$read)
            }
            while($read -ne 0)
            }
        catch
        {
            throw
        }
        finally
        {
            #cleanup
            if($fsIn)
            {
                $fsIn.Close()
                $fsIn.Dispose()
            }
            if($gzStream)
            {
                $gzStream.Close()
                $gzStream.Dispose()
            }
            if($fsOut)
            {
                $fsOut.Close()
                $fsOut.Dispose()
            }
        }
    }


####################################################################################
####################################################################################

    function GetTransferTables
    {
        param
        (
        [string]$ManufacturerID
        )

        [string]$lasterr = $null;

        #container for tables & sql text
        $allfiletables = @{}

        Try
        {
            $allfiletables.Add("Attribute", @{sqltext=""});
            $allfiletables.Add("AttributeLayout", @{sqltext=""})
            $allfiletables.Add("AttributeTab", @{sqltext=""});
            $allfiletables.Add("Barcodes", @{sqltext=""});
            $allfiletables.Add("Config", @{sqltext=""});
            $allfiletables.Add("ConsumeReasons", @{sqltext=""});
            $allfiletables.Add("DepartmentProductData2", @{sqltext=""});
            $allfiletables.Add("InsuranceStatus", @{sqltext=""});
            $allfiletables.Add("Lot", @{sqltext=""});
            $allfiletables.Add("Physicians", @{sqltext=""});
            $allfiletables.Add("PhysicianDepartment2", @{sqltext=""});
            $allfiletables.Add("Products", @{sqltext=""});
            $allfiletables.Add("Reasons", @{sqltext=""});
            $allfiletables.Add("Room", @{sqltext=""});
            $allfiletables.Add("RoomCart", @{sqltext=""});
            $allfiletables.Add("SiteProductLot", @{sqltext=""});
            $allfiletables.Add("Staff", @{sqltext=""});
            $allfiletables.Add("StaffDepartment", @{sqltext=""});
            $allfiletables.Add("StaffLabel", @{sqltext=""});
            $allfiletables.Add("StaffType", @{sqltext=""});
            $allfiletables.Add("TechDepartment2", @{sqltext=""});
            $allfiletables.Add("Techs", @{sqltext=""});
            $allfiletables.Add("User", @{sqltext=""});
            $allfiletables.Add("VTMButton", @{sqltext=""});
            $allfiletables.Add("VTMLabel", @{sqltext=""});
            $allfiletables.Add("VTMPage", @{sqltext=""});
            $allfiletables.Add("DataBag", @{sqltext=""});
            $allfiletables.Add("EPC", @{sqltext=""});
            $allfiletables.Add("EPCLot", @{sqltext=""});
            $allfiletables.Add("Site", @{sqltext=""});
            $allfiletables.Add("Department", @{sqltext=""});
            $allfiletables.Add("RoomType", @{sqltext=""});
            $allfiletables.Add("Room_RoomType", @{sqltext=""});
            $allfiletables.Add("RoomTypeDimensionOrder", @{sqltext=""});
            $allfiletables.Add("RoomTypeOrder", @{sqltext=""});

            # min stuff first
            $allfiletables["Barcodes"]["sqltext"] = "SELECT b.BarcodeID, b.ProductID, ISNULL(p.ManufacturerProductCode,'') as ManufacturerProductCode, UPNCode, left(UPNCode, LEN(UPNCode) - 1) AS UPNCodeMinus1, LEFT(UPNCode, len(UPNCode) - 2) AS UPNCodeMinus2, Quantity FROM barcode b INNER JOIN product p ON b.productID = p.productID WHERE b.IsActive > 0 AND p.IsActive > 0 AND p.ProductID > 0  and (('$ManufacturerID' = '-1') or (ManufacturerID in ($ManufacturerID))) ORDER BY UPNCode";
            $allfiletables["Products"]["sqltext"] = "SELECT ProductID, ISNULL(CategoryName,'') as Category, ISNULL(ManufacturerName,'') as Manufacturer, ISNULL(ProductType,'') as Type, ISNULL(ProductName,'') as Name, ISNULL(Diameter,'') as Diameter, ISNULL(Length, '') as Length, ISNULL(PrimaryFeature,'') as PrimaryFeature, ISNULL(SecondaryFeature,'') as SecondaryFeature, ISNULL(OtherFeature,'') as OtherFeature, LotTypeID as LotType, ISNULL(ManufacturerProductCode,'') as ManufacturerProductCode2 FROM ProductView pv WHERE productID >= 0 and (('$ManufacturerID' = '-1') or (ManufacturerID in ($ManufacturerID))) ORDER BY ManufacturerName, CategoryName, ProductType, ProductName";
            $allfiletables["DepartmentProductData2"]["sqltext"] = "SELECT DepartmentProductDataID, SiteID, DepartmentID, ProductID, cast(IgnoreLot as int) AS IgnoreLot, ToTag FROM DepartmentProductData WHERE ((IgnoreLot > 0) or (ToTag >= 0)) and (($SiteID = -1) or (SiteID = $SiteID)) ORDER BY ProductID";
            $allfiletables["DataBag"]["sqltext"] = "SELECT DataBagID, Data, [Key], SiteID, DepartmentID, RoomID FROM DataBag WHERE  (($SiteID = -1) or (SiteID = $SiteID))";

            if ($min -eq $false)
            {
                $allfiletables["Attribute"]["sqltext"] = "SELECT AttributeID, SiteID, DepartmentID, ISNULL(Name,'') as Name, ISNULL(Description,'') as Description, ISNULL(BillingCode,'') as BillingCode, ISNULL(ServiceCode,'') as ServiceCode FROM Attribute WHERE IsActive > 0 and (($SiteID = -1) or (SiteID = $SiteID))";
                $allfiletables["AttributeLayout"]["sqltext"] = "SELECT al.AttributeLayoutID, al.AttributeID, al.AttributeTabID, al.[Column], al.[Order], al.Indent FROM AttributeLayout al INNER join Attribute a on a.AttributeID = al.AttributeID WHERE al.IsActive > 0 and (($SiteID = -1) or (SiteID = $SiteID))";
                $allfiletables["AttributeTab"]["sqltext"] = "SELECT AttributeTabID, SiteID, DepartmentID, [Order], Name, Color, Columns FROM AttributeTab WHERE IsActive > 0 and (($SiteID = -1) or (SiteID = $SiteID))";
                $allfiletables["Config"]["sqltext"] = "SELECT ProductCatalogVersion+1 AS ProductCatalogVersion, $flavor as Flavor FROM Config";
                $allfiletables["ConsumeReasons"]["sqltext"] = "SELECT ConsumeReasonID, SiteID, Description as Reason FROM ConsumeReason WHERE IsActive > 0 and (($SiteID = -1) or (SiteID = $SiteID))";
                $allfiletables["InsuranceStatus"]["sqltext"] = "SELECT InsuranceStatusID, Name, SiteID FROM InsuranceStatus WHERE (($SiteID = -1) or (SiteID = $SiteID))";

                if ($rfid -eq $false)
                {
                    $allfiletables["Lot"]["sqltext"] = "SELECT l.LotID as id, l.LotNumber as lot_number, l.ExpirationDate as expiration, l.productid FROM Lot l WHERE l.LotID in (SELECT DISTINCT LotID FROM SiteProductLot) and ('$ManufacturerID' = '-1') ORDER BY l.LotNumber";
                }	
                else
                { 
                    #RFID option enabled
                    $allfiletables["Lot"]["sqltext"] = "SELECT l.LotID as id, l.LotNumber as lot_number, l.ExpirationDate as expiration, l.productid FROM Lot l WHERE l.LotID in (SELECT DISTINCT LotID FROM SiteProductLot union all SELECT distinct LotID from EPCLot) and ('$ManufacturerID' = '-1') ORDER BY l.LotNumber";
                }

                $allfiletables["Physicians"]["sqltext"] = "SELECT PhysicianID, SiteID as Site, Name, cast(IsActive as int) as Active FROM Physician WHERE IsActive > 0 and (($SiteID = -1) or (SiteID = $SiteID)) ORDER BY SiteID, PhysicianID";
                $allfiletables["PhysicianDepartment2"]["sqltext"] = "SELECT PhysicianID, DepartmentID, SiteID, cast(IsActive as int) as IsActive FROM PhysicianDepartment WHERE IsActive > 0 and (($SiteID = -1) or (SiteID = $SiteID)) ORDER BY SiteID, DepartmentID, PhysicianID";
                $allfiletables["Reasons"]["sqltext"] = "SELECT ReasonID, SiteID, Description as Reason FROM Reason WHERE IsActive > 0  and (($SiteID = -1) or (SiteID = $SiteID)) and IsRemoval > 0";
                $allfiletables["Room"]["sqltext"] = "SELECT RoomID, SiteID, DepartmentID, Name, 1 as Active, ISNULL(VTMHomePageID,'') as VTMHomePageID FROM Room WHERE IsActive > 0 AND IsCart = 0 and (($SiteID = -1) or (SiteID = $SiteID)) ORDER BY Name";
                $allfiletables["SiteProductLot"]["sqltext"] = "SELECT sp.SiteID as site, sp.ProductID as item, spl.LotID as lot, spl.trusted FROM SiteProductLot spl INNER JOIN SiteProduct sp on spl.SiteProductID = sp.SiteProductID WHERE (($SiteID = -1) or (SiteID = $SiteID))";
                $allfiletables["Staff"]["sqltext"] = "SELECT StaffID, SiteID,Name, CAST(IsActive AS int) AS Active, StaffTypeID FROM Staff WHERE (($SiteID = -1) or (SiteID = $SiteID)) ORDER BY SiteID, StaffID";
                $allfiletables["StaffDepartment"]["sqltext"] = "SELECT StaffID, DepartmentID, cast(IsActive as int) as IsActive, SiteID FROM StaffDepartment WHERE (($SiteID = -1) or (SiteID = $SiteID)) ORDER BY SiteID, DepartmentID, StaffID";
                $allfiletables["StaffLabel"]["sqltext"] = "SELECT SiteID, StaffTypeID, Label FROM StaffLabel WHERE (($SiteID = -1) or (SiteID = $SiteID))";
                $allfiletables["StaffType"]["sqltext"] = "SELECT StaffTypeID, Name FROM StaffType";
                $allfiletables["TechDepartment2"]["sqltext"] = "SELECT TechID, DepartmentID, SiteID, cast(IsActive as int) as IsActive FROM TechDepartment WHERE IsActive > 0 and (($SiteID = -1) or (SiteID = $SiteID)) ORDER BY SiteID, DepartmentID, TechID";
                $allfiletables["Techs"]["sqltext"] = "SELECT TechID, SiteID as Site, Name, cast(IsActive as int) as Active FROM  Tech WHERE IsActive > 0 and (($SiteID = -1) or (SiteID = $SiteID)) ORDER BY SiteID,  TechID";
                $allfiletables["User"]["sqltext"] = "SELECT UserID, FullName, UserName, SiteID, PIN FROM [user] WHERE pin > 0 and (($SiteID = -1) or (SiteID = $SiteID))";
                $allfiletables["VTMButton"]["sqltext"] = "SELECT VTMButtonID, VTMPageID, BackColor, TextColor, LocationX, LocationY, SizeX, SizeY, Font, Text, VTMActionID, NextPageID, FromID, ToID, ScanLabel, ScanMessageFont, ScanConfirmMessage, ScanCancelMessage, ScanResumeMessage, ScanBackgroundColor FROM VTMButton WHERE IsActive > 0";
                $allfiletables["VTMLabel"]["sqltext"] = "SELECT VTMLabelID, VTMPageID, TextColor, LocationX, LocationY, Font, Text FROM VTMLabel WHERE IsActive > 0";
                $allfiletables["VTMPage"]["sqltext"] = "SELECT VTMPageID, RoomID, BackgroundColor, Name, Description FROM VTMPage WHERE IsActive > 0";									
                $allfiletables["Department"]["sqltext"] = "SELECT DepartmentID, Name FROM Department;";

                #New for RFID
                if ($rfid -eq $true)
                {
                    $allfiletables["EPC"]["sqltext"] = "SELECT EPCID, EPC, tagID FROM EPC WHERE EPCID IN (SELECT EPCID FROM EPCLot)";
                    $allfiletables["EPCLot"]["sqltext"] = "SELECT EPCID, LotID, VUEMEDSerial, Active FROM EPCLot;";
        
                    #InventoryModel
                    $allfiletables["Site"]["sqltext"] = "SELECT SiteID, Name, MobileActive FROM Site where IsActive = 1;";
                    $allfiletables["RoomType"]["sqltext"] = "SELECT RoomTypeID, SiteID, Name, Dimension FROM RoomType;";
                    $allfiletables["Room_RoomType"]["sqltext"] = "SELECT Room_RoomTypeID, RoomTypeID, RoomID FROM Room_RoomType;";
                    $allfiletables["RoomTypeDimensionOrder"]["sqltext"] = "SELECT RoomTypeDimensionOrderID, RoomTypeID, RoomTypeID2, RoomID, [Order] FROM RoomTypeDimensionOrder;";
                    $allfiletables["RoomTypeOrder"]["sqltext"] = "SELECT RoomTypeOrderID, RoomTypeID, [Order], SiteID FROM RoomTypeOrder;";
                }
            }
        }
        Catch
        {
            $lasterr = $error[0].Exception.Message
            Write-Error -Exception $error[0]
        }
        Finally
        {
        
        }

        #return data set (containing tables) and sql out params
        return @{data=$allfiletables;lasterror=$lasterr}
    }

####################################################################################
####################################################################################


    Function CreatCatalog
    {
        param
        (
            [int]$flavor = 1, 
            [int[]]$ManufacturerIDs = -1, 
            [int]$SiteID= -1, 
            [bool]$rfid = $false, 
            [bool]$min = $false
        )

        $ManufacturerID = ($ManufacturerIDs|group|Select -ExpandProperty Name) -join ","
        $isDefault = $false

        if ($flavor -eq 1) 
        {
            $isDefault = $true
        }

        $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
        $command = new-object system.data.sqlclient.sqlcommand("",$connection)
        $connection.Open()


# if ($min -eq $false) # $min is always false - no call ever sets it to true
# {
        if ($isDefault)
        {
            Try
            {
                $command.CommandText = "Exec UpdateSiteProductLot" 
                $command.CommandTimeout = 3600
                $command.ExecuteNonQuery()
            }
            Catch
            {
                Write-Output $error
                $connection.Close()
                return
            }
        }
#}

        # go get the queries for this flavor
        $res = GetTransferTables -ManufacturerID $ManufacturerID

        $allfiletables = $res.data;
        $mastervuemedds = new-object System.Data.DataSet;

        #fill datatables with data from queries
        foreach($key in $allfiletables.Keys)
        {
            $readquery = $allfiletables[$key]["sqltext"]
           
            if ($readquery -ne "")
            {
                $sqladapter = New-Object System.Data.SqlClient.SqlDataAdapter($readquery, $connection)
                $ds = New-Object System.Data.DataSet
                $sqladapter.AcceptChangesDuringFill = $false;
                $sqladapter.Fill($ds)

                if($ds.Tables.Count -eq 1)
                {
                    $dt = $ds.Tables[0]

                    if ($dt.Rows.Count -gt 0)
                    {
                        $dt.TableName = $key

                        try
                        {
                            $ds.Tables.Remove($dt)
                            
                            $mastervuemedds.Tables.Add($dt)
                        }
                        catch
                        {
                            write-output $error
                        }
                    }
                }
            }
        }

        $connection.Close()

        $StorageKeytype = "StorageAccessKey"
        $context = New-AzureStorageContext -StorageAccountName "vuemedblob" -StorageAccountKey $StorageKeyBlob
        
        # get schema text
        $blob = Get-AzureStorageBlob -Context $context -Container 'product-catalog-resources' -Blob 'ProductCatalogCreate_Schema.sql'
        $schemaQuery = $blob.ICloudBlob.DownloadText()
        
        $timestamp = [System.DateTime]::Now.ToString("yyyyMMddHHmmssfff")

        #names of dbs we're producing when placed in storage
        if ($flavor -eq 1)
        {
            $thisdb = "ProductCatalog.db"
        }
        else ############# not doing these anymore
        {
            $thisdb = "ProductCatalogMerge-$flavor-$timestamp.db"
        }
        
        $thisdbgz = "ProductCatalogMerge-$flavor-$timestamp.gz"


        # location of temporary local storage
        $myEnv = $env:TEMP
        $database = "$myEnv\$thisdb"

        #SQLite will create db and schema all in one shot 
        Invoke-SqliteQuery -Query $schemaQuery -DataSource $database

        # TRANSFER DATASET TABLE DATA INTO NEW SQLITE DB FILE #
        foreach($dataTable in $mastervuemedds.Tables)
        {
            Try
            {
                $connStr = "Data Source = $database"
    
                $conn = New-Object System.Data.SQLite.SQLiteConnection($connStr)

                $conn.Open()

                $transaction1 = $conn.BeginTransaction();

                $tablename = $dataTable.TableName

                $sqliteAdapter = New-Object System.Data.SQLite.SQLiteDataAdapter("SELECT * FROM $tablename", $conn)
                                
                $cmdBuilder = New-Object System.Data.SQLite.SQLiteCommandBuilder($sqliteAdapter)
                
                $sqliteAdapter.InsertCommand = $cmdBuilder.GetInsertCommand();
                                
                #Without the SQLiteCommandBuilder this line would fail
                $insertedrows = $sqliteAdapter.Update($dataTable);

                $dataTable.AcceptChanges();

                $transaction1.Commit();
            }
            Catch
            {
                Write-Output $error

                $transaction1.Rollback();

                return
            }
            Finally
            {
                $conn.close()

                $sqliteAdapter.Dispose();
            }
        }

        # create views
        $blob = Get-AzureStorageBlob -Context $context -Container 'product-catalog-resources' -Blob 'ProductCatalogCreate_Views.sql'
        $viewQuery = $blob.ICloudBlob.DownloadText()
        Invoke-SqliteQuery -Query $viewQuery -DataSource $database

        # create indicies
        $blob = Get-AzureStorageBlob -Context $context -Container 'product-catalog-resources' -Blob 'ProductCatalogCreate_Indexes.sql'
        $indexQuery = $blob.ICloudBlob.DownloadText()
        Invoke-SqliteQuery -Query $indexQuery -DataSource $database


        #send our new db to blob storage - it will be deleted from temp when runbook finishes
        if ($flavor -eq 1)
        {
            # force an overwrite - this one is Product Catalog - no flavor
            Set-AzureStorageBlobContent -Context $context -Container 'product-catalog' -File $Database -Blob $thisdb -Force
        }
        else
        {
            # flavors and flavors gzipped
            #                                   not doing these anymore
            #Set-AzureStorageBlobContent -Context $context -Container 'product-catalog' -File $Database -Blob $thisdb
        
            $gzippeddb = $Database.Replace(".db", ".gz")

            #gzip it
            GZip-File -fileToCompress $Database -compressedFile $gzippeddb
            # and send to storage
            Set-AzureStorageBlobContent -Context $context -Container 'product-catalog' -File $gzippeddb -Blob $thisdbgz
        }
        
        #if flavor = 1, it's the last one made - get the new catalog version
        if ($flavor -eq 1)
        {
            Try
            {
                $connStr = "Data Source = $database"
                $conn = New-Object System.Data.SQLite.SQLiteConnection($connStr)
                $conn.Open()
                $sqliteCommand = $conn.CreateCommand()
                $sqliteCommand.CommandText = "Select ProductCatalogVersion From Config"
                $SqliteDataReader = $sqliteCommand.ExecuteReader()
                $SqliteDataReader.Read() 
                $newCatalogNumber = $SqliteDataReader.GetInt32(0)
            }
            Catch
            {
                Write-Output $error

                return
            }
            Finally
            {
                $conn.Close()
                $sqliteCommand.Dispose()
                $SqliteDataReader.Dispose()
            }

            # update prod db with new catalog number
            Try
            {
                $connection.Open()
                $command.CommandText = "UpdateProductCatalogVersion" 
                $command.CommandType = [System.Data.CommandType]'StoredProcedure'
                $command.Parameters.Clear()
                $command.Parameters.AddWithValue("@newNumber", $newCatalogNumber)
                $command.CommandTimeout = 3600
                $command.ExecuteNonQuery()
            }
            Catch
            {
                Write-Output $error
                
                return
            }
            Finally
            {
                $connection.Close()
            }
        }


        #delete from temp
        Remove-Item -Path $database -Force

        if ($flavor -ne 1)
        {
            Remove-Item -Path $gzippeddb -Force
        }

        #update sql database with new file name
        if ($flavor -eq 1)
        {
            $location = "https://vuemedblob.blob.core.windows.net/product-catalog/ProductCatalog.db"
        }
        else
        {
            $location = "https://vuemedblob.blob.core.windows.net/product-catalog/" + $thisdbgz
        }
        
        $dic = New-Object 'system.collections.generic.dictionary[int,string]'
        $dic[1] = "ProductCatalog.db"
        $dic[2] = "ProductCatalogMin.db"
        $dic[7] = "ProductCatalogMerge-7.gz"
        $dic[8] = "ProductCatalogMerge-8.gz"
        $dic[9] = "ProductCatalogMerge-9.gz"
        $dic[10] = "ProductCatalogMerge-10.gz"
        $dic[11] = "ProductCatalogMerge-11.gz"
        $dic[12] = "ProductCatalogMerge-12.gz"


        Try
        {
            $filename = $dic[$flavor]

            $connection.Open()
            $command.CommandText = "UpdateProductCatalogLocations" 
            $command.CommandType = [System.Data.CommandType]'StoredProcedure'
            $command.Parameters.Clear()
            $command.Parameters.AddWithValue("@filename", $filename)
            $command.Parameters.AddWithValue("@instanceFileName", $location)
            $command.CommandTimeout = 3600
            $command.ExecuteNonQuery()
        }
        Catch
        {
            Write-Output $error
            
            return
        }
        Finally
        {
            $connection.Close()
        }
    }

####################################################################################
####################################################################################

# here's the actual calls - all above are functions

Write-Output "Before call"
Write-Output $(Get-Date)

    CreatCatalog -flavor 8  -SiteID 35  -ManufacturerID 5,11,98,413,441,452,498,5272,555
    CreatCatalog -flavor 7  -SiteID 33  -ManufacturerID 4,5368,5360,8,39,2752,445,139,188,504,331,312,267,255,105,459,481,240,5370,101,555
    CreatCatalog -flavor 2  -SiteID 0   -ManufacturerID 0
    CreatCatalog -flavor 9  -SiteID 36  -ManufacturerID 10
    CreatCatalog -flavor 10 -SiteID 37 -ManufacturerID 19,572,5370,89
    CreatCatalog -flavor 11 -SiteID 38 -ManufacturerID 21
    CreatCatalog -flavor 12 -Rfid $true
    CreatCatalog

Write-Output "After call"
Write-Output $(Get-Date)



