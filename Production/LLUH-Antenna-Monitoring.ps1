
    $Conn = Get-AutomationConnection -Name AzureRunAsConnection
    Connect-AzureRmAccount -ServicePrincipal -Tenant $Conn.TenantID -ApplicationId $Conn.ApplicationID `
    -CertificateThumbprint $Conn.CertificateThumbprint

    $settingsXml = Get-AutomationVariable -Name SettingsXML
    [xml]$ConfigFile = $settingsXml

    $SendGridUsername = $ConfigFile.Settings.Email.SendGridUserName
    $SendGridPassword = ConvertTo-SecureString -String $ConfigFile.Settings.Email.SendGridUserPassword -AsPlainText -Force
    $credential = New-Object System.Management.Automation.PSCredential $SendGridUsername, $SendGridPassword
    $SMTPServer = $ConfigFile.Settings.Email.SMTPServer
    $EmailFrom = $ConfigFile.Settings.Email.emailFromSupport
    $EmailTo = $ConfigFile.Settings.Email.emailToSupport
    $Subject = "LLUH Antenna Event Monitoring"

    $connectionString = $ConfigFile.Settings.Database.vuemed_production_sql_server_vuemed_connection_string
    $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
    $command = new-object system.data.sqlclient.sqlcommand("",$connection)
    $connection.Open()

    $command.CommandText = 'EXEC AntennaMonitoringAndEmailAzure '

    $command.CommandTimeout = 120
    
    $val = -1

    try
    {
        $val = [int]$command.ExecuteScalar()
    }
    Finally
    {
        $connection.Close()
    }

    if ($val -gt 0)
    {
        $Body = "<p> Reads in the last 15 minutes:</p><p> -----------</p><p> $val</p>"
        Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
     }
    







