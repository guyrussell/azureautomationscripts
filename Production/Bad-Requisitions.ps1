    $settingsXml = Get-AutomationVariable -Name SettingsXML
    [xml]$ConfigFile = $settingsXml

    $Conn = Get-AutomationConnection -Name AzureRunAsConnection
    Connect-AzureRmAccount -ServicePrincipal -Tenant $Conn.TenantID -ApplicationId $Conn.ApplicationID `
    -CertificateThumbprint $Conn.CertificateThumbprint

    $connectionString = $ConfigFile.Settings.Database.vuemed_production_sql_server_vuemed_connection_string
    $EmailFrom = $ConfigFile.Settings.Email.emailFromOps
    $EmailTo = $ConfigFile.Settings.Email.emailToSupport
    $SendGridUsername = $ConfigFile.Settings.Email.SendGridUserName
    $SendGridPassword = ConvertTo-SecureString -String $ConfigFile.Settings.Email.SendGridUserPassword -AsPlainText -Force
    #change this to SendGridServer?
    $SMTPServer = $ConfigFile.Settings.Email.SMTPServer
   
    $credential = New-Object System.Management.Automation.PSCredential $SendGridUsername, $SendGridPassword
    $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
    $command = new-object system.data.sqlclient.sqlcommand("",$connection)
    $command.CommandText = 'EXEC BadRequisitionsWMC'
    $command.CommandTimeout = 120
    $connection.Open()

    $val = ''

    try
    {
        $val = $command.ExecuteScalar()
    }
    Finally
    {
        $connection.Close()
    }

    if ($val.length -gt 0)
    {
        $Subject = "WMC - There are products that cannot be requisitioned"
        $Body ="<p><a clicktracking=off href=`"https://v5.vuemed.com/en-US/reports/productsbadrequisitionsdata`">https://v5.vuemed.com/en-US/reports/productsbadrequisitionsdata</a></p><p> NumberOfProducts</p><p> -----------</p><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$val</p>"
        Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
    }

#############################################################################

    $command.CommandText = 'EXEC BadRequisitionsLLUH'

    $command.CommandTimeout = 120
    $connection.Open()
    
    $val = ''

    try
    {
        $val = $command.ExecuteScalar()
    }
    Finally
    {
        $connection.Close()
    }

    if ($val.length -gt 0)
    {
        $Subject = "LLUH - There are products that cannot be requisitioned"
        $Body ="<p><a clicktracking=off href=`"https://v5.vuemed.com/en-US/reports/productsbadrequisitionsdata`">https://v5.vuemed.com/en-US/reports/productsbadrequisitionsdata</a></p><p> NumberOfItems</p><p> -----------</p><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$val</p>"
        Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
    }



