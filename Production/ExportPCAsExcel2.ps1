

    $settingsXml = Get-AutomationVariable -Name SettingsXML
    [xml]$ConfigFile = $settingsXml

    $Conn = Get-AutomationConnection -Name AzureRunAsConnection
    Connect-AzureRmAccount -ServicePrincipal -Tenant $Conn.TenantID -ApplicationId $Conn.ApplicationID `
    -CertificateThumbprint $Conn.CertificateThumbprint

    $StorageKey = $ConfigFile.Settings.Storage.vuemed_blob_primary_access_key
    $connectionString = $ConfigFile.Settings.Database.vuemed_production_sql_server_vuemed_connection_string

    Import-Module ImportExcel

    $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
    $command = new-object system.data.sqlclient.sqlcommand("",$connection)
    $connection.Open()

    $timestamp = ($(Get-Date).AddHours(-7)).ToString("yyyyMMddHHmmssfff")
    $fileName = "ProductCatalogExport-$timestamp.xlsx"
    $toFile = "$env:TEMP\$fileName"

    Write-Output "started"
    Write-Output $(Get-Date)


    Try
    {
        $query = " Exec GetExistingProductsForExcelImport2" 
        $sqladapter = New-Object System.Data.SqlClient.SqlDataAdapter($query, $connection)
        $sqladapter.SelectCommand.CommandTimeout = 7200;
        $ds = New-Object System.Data.DataSet
        $sqladapter.Fill($ds)
        $ds.Tables[0] | Export-Excel -Path $toFile -Verbose:$false -WorksheetName "AllExistingProducts" -NoNumberConversion 'ProductID', 'BarcodeID', 'ManufacturerName', 'CategoryName', 'ProductType', 'ProductName', 'ManufacturerProductCode', 'Diameter', 'Length', 'PrimaryFeature', 'SecondaryFeature', 'OtherFeature', 'LotTypeID', 'UPNCode', 'Quantity', 'Comment1', 'Comment2', 'Comment3', 'Comment4' -ExcludeProperty 'RowError', 'RowState', 'Table', 'ItemArray', 'HasErrors' -BoldTopRow -FreezeTopRow -AutoSize
    }
    Catch
    {
        #probably should send an email here?
        Write-Output $error
        $connection.Close()
        return
    }

    
    $context = New-AzureStorageContext -StorageAccountName "vuemedblob" -StorageAccountKey $StorageKey

    #export to blob storage
    Set-AzureStorageBlobContent -Context $context -Container 'product-catalog' -File $toFile -Blob $fileName
        
    Write-Output "finished"
    Write-Output $(Get-Date)









