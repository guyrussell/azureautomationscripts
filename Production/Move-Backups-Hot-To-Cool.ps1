

    $Conn = Get-AutomationConnection -Name AzureRunAsConnection
    Connect-AzureRmAccount -ServicePrincipal -Tenant $Conn.TenantID -ApplicationId $Conn.ApplicationID `
    -CertificateThumbprint $Conn.CertificateThumbprint

    $settingsXml = Get-AutomationVariable -Name SettingsXML
    [xml]$ConfigFile = $settingsXml
    
    $StorageKeySource = $ConfigFile.Settings.Storage.databasebackuphot_storage_access_key

    $contextSource = New-AzureStorageContext -StorageAccountName 'databasebackuphot' -StorageAccountKey $StorageKeySource
    $date = ((Get-Date).AddDays(-7)).ToString("yyyy_MM_dd")
        
    $blobVueMed = Get-AzureStorageBlob -Container 'backups' -Context $contextSource -Blob 'VueMed_backup_*' | Sort Name -Descending  | where {$_.Name -lt "VueMed_backup_$date*"} | Select-Object -First 1
    $blobVueMed.ICloudBlob.SetStandardBlobTier("Cool")

    $blobItemMaster = Get-AzureStorageBlob -Container 'backups' -Context $contextSource -Blob 'ItemMaster_backup_*' | Sort Name -Descending | where {$_.Name -lt "ItemMaster_backup_$date*"} | Select-Object -First 1
    $blobItemMaster.ICloudBlob.SetStandardBlobTier("Cool")
        
    $blobSync = Get-AzureStorageBlob -Container 'backups' -Context $contextSource -Blob 'VueMedSync_v4_backup_*' | Sort Name -Descending | where {$_.Name -lt "VueMedSync_v4_backup_$date*"} | Select-Object -First 1
    $blobSync.ICloudBlob.SetStandardBlobTier("Cool")

   
