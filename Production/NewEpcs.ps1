

    $Conn = Get-AutomationConnection -Name AzureRunAsConnection
    Connect-AzureRmAccount -ServicePrincipal -Tenant $Conn.TenantID -ApplicationId $Conn.ApplicationID `
    -CertificateThumbprint $Conn.CertificateThumbprint

    $settingsXml = Get-AutomationVariable -Name SettingsXML
    [xml]$ConfigFile = $settingsXml

    #Import-Module PSExcel
    Import-Module ImportExcel

    $StorageKey = $ConfigFile.Settings.Storage.vuemed_blob_primary_access_key
    $connectionString = $ConfigFile.Settings.Database.vuemed_production_sql_server_vuemed_connection_string

    $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
    $command = new-object system.data.sqlclient.sqlcommand("",$connection)
    $connection.Open()

    $timestamp = ($(Get-Date).AddHours(-7)).ToString("yyyyMMddHHmmssfff")
    $fileName = "NewEPCs-$timestamp.xlsx"
    $toFile = "$env:TEMP\$fileName"

    Write-Output "started"
    Write-Output $(Get-Date)


    Try
    {
        $query = "Exec NewEpcs" 
        $sqladapter = New-Object System.Data.SqlClient.SqlDataAdapter($query, $connection)
        $ds = New-Object System.Data.DataSet
        $command.CommandTimeout = 3600
        $sqladapter.Fill($ds)
        $ds.Tables[0] | Export-Excel -Path $toFile -Verbose:$false -NoNumberConversion 'AntennaEventID', 'EPCID', 'epc', 'VuemedSerial', 'FirstSeenTime', 'ProductID', 'RoomID', 'name', 'ProductName', 'manufacturername', 'ManufacturerProductCode' -ExcludeProperty 'RowError', 'RowState', 'Table', 'ItemArray', 'HasErrors' -BoldTopRow -FreezeTopRow -AutoSize
    }
    Catch
    {
        #used to send an email here
        Write-Output $error
        $connection.Close()
        return
    }

   
    $context = New-AzureStorageContext -StorageAccountName "vuemedblob" -StorageAccountKey $StorageKey

    #export to blob storage
    Set-AzureStorageBlobContent -Context $context -Container 'newepcs' -File $toFile -Blob $fileName
        
    Write-Output "finished"
    Write-Output $(Get-Date)












