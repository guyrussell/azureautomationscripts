
    $settingsXml = Get-AutomationVariable -Name SettingsXML
    [xml]$xmlcontent = $settingsXml

    $SendGridUsername = $xmlcontent.Settings.General.SendGridUserName
    $SendGridPassword = ConvertTo-SecureString -String $xmlcontent.Settings.General.SendGridUserPassword -AsPlainText -Force
    $EmailTo = $xmlcontent.Settings.General.EmailToSupport
    $connectionString = $xmlcontent.Settings.General.VMSqlRemoteConnectionString

    $credential = New-Object System.Management.Automation.PSCredential $SendGridUsername, $SendGridPassword
    $SMTPServer = "smtp.sendgrid.net"
    $EmailFrom = "support+azure@vuemed.com"
    $Subject = "Requistions Created in last 12 Hours"

    $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
    $commandText = 'EXEC UnackedRequisitions'
    $command = new-object system.data.sqlclient.sqlcommand($commandText, $connection)
    $command.CommandTimeout = 3600
    $connection.Open()
    
    $val1 = 0
    $val2 = 0

    #Select @Unacked, @Created
    try
    {
        $reader = $command.ExecuteReader()
       
        $reader.Read()
        $val1 = $reader[0]
        $val2 = $reader[1]
    }
    Finally
    {
        $connection.Close()
    }

    $day = (Get-Date).DayOfWeek.value__
    $hour = (Get-Date).Hour - 7.5 #daylight savings or not

    if ($val1 -gt 0)
    {
        $Body ="<p> Todays Requisition count : $val2</p><p> Unacked : $val1</p>"
        Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
    }
    elseif ($day -eq 5 -and ($hour -gt 11 -and $hour -lt 13))
    {
        $Body ="<p> Todays Requisition count : $val2</p><p> Unacked : $val1</p><br>"
        $Body +="<p> Weekly ping to report that there's nothing to report<br> but that's something to report so there IS something to report<br> which is, there's nothing to report</p>"
        Send-MailMessage -smtpServer $SMTPServer -Credential $credential -Usessl -Port 587 -from $EmailFrom -to $EmailTo -subject $Subject -Body $Body -BodyAsHtml
    }





