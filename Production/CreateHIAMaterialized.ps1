
   $settingsXml = Get-AutomationVariable -Name SettingsXML
    [xml]$ConfigFile = $settingsXml

    $Conn = Get-AutomationConnection -Name AzureRunAsConnection
    Connect-AzureRmAccount -ServicePrincipal -Tenant $Conn.TenantID -ApplicationId $Conn.ApplicationID `
    -CertificateThumbprint $Conn.CertificateThumbprint

    $connectionString = $ConfigFile.Settings.Database.vuemed_production_sql_server_vuemed_connection_string

    $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
    $command = new-object system.data.sqlclient.sqlcommand("",$connection)
    $connection.Open()

    $command.CommandText = 'EXEC CreateHIAMaterialized'

    $command.CommandTimeout = 3600
    
    Try
    {
        $command.ExecuteNonQuery()

        Write-Output 'Success!'
    }
    Catch
    {
        Write-Output 'Failure!'
    }
    Finally
    {
        $connection.Close();
    }


