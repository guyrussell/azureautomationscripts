

    $settingsXml = Get-AutomationVariable -Name SettingsXML
    [xml]$ConfigFile = $settingsXml

    $Conn = Get-AutomationConnection -Name AzureRunAsConnection
    Connect-AzureRmAccount -ServicePrincipal -Tenant $Conn.TenantID -ApplicationId $Conn.ApplicationID `
    -CertificateThumbprint $Conn.CertificateThumbprint

    $connectionString = $ConfigFile.Settings.Database.vuemed_production_sql_server_vuemed_connection_string
    $StorageKeyBlob = $ConfigFile.Settings.Storage.vuemed_blob_primary_access_key
    $StorageKeyBID = $ConfigFile.Settings.Storage.bidmcadmin_storage_primary_access_key

    $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
    $context = New-AzureStorageContext -StorageAccountName "vuemedblob" -StorageAccountKey $StorageKeyBlob
    $blob = Get-AzureStorageBlob -Context $context -Container "sql-scripts" -Blob "homer.sql"
    $query = $blob.ICloudBlob.DownloadText()
    
    $command = new-object system.data.sqlclient.sqlcommand($query, $connection)
    $command.CommandTimeout = 3600

    $date = (get-date).AddHours(-7).ToString("yyyyMMddHHmm")
    $loc = $env:TEMP
    $fileName = "BIDMCCARDIO_" + $date + ".csv"
    $extractFile =  $loc + $fileName

    #access bidmcadmin storage to place file
    $context = New-AzureStorageContext -StorageAccountName "bidmcadmin" -StorageAccountKey $StorageKeyBID

    Try
    {
        $connection.Open()
        $SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter
        $SqlAdapter.SelectCommand = $command
        $DataSet = New-Object System.Data.DataSet
        $SqlAdapter.Fill($DataSet)

        $DataSet.Tables[0] | Export-Csv -NoTypeInformation $extractFile

        Set-AzureStorageBlobContent -File $extractFile -Container "bidmcftp" -Blob $fileName -Context $context -Force
    
        Write-Output "Success!"
    }
    Catch
    {
        Write-Output "Failure!"

        Write-Output $error
    }
    Finally
    {
        $connection.Close()
    }
    



